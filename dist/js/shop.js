$(document).ready(function() {
  
  var vidHtml = "<div class=\"video-wrapper\"><iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/zOZ3pkp-QPQ?rel=0&amp;controls=0&autoplay=1\" frameborder=\"0\" allowfullscreen></iframe></div>";
  
  $('.flexb').click(function() {
    console.log("clicked!");
   	$('.flexb').html(vidHtml);
  });
  
  
	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
  
    // Find all YouTube videos
var $allVideos = $("iframe[src^='//www.youtube.com']"),

    // The element that is fluid width
    $fluidEl = $("body");

// Figure out and save aspect ratio for each video
$allVideos.each(function() {

  $(this)
    .data('aspectRatio', this.height / this.width)

    // and remove the hard coded width/height
    .removeAttr('height')
    .removeAttr('width');

});

// When the window is resized
$(window).resize(function() {

  var newWidth = $fluidEl.width();

  // Resize all videos according to their own aspect ratio
  $allVideos.each(function() {

    var $el = $(this);
    $el
      .width(newWidth)
      .height(newWidth * $el.data('aspectRatio'));

  });

// Kick off one resize to fix all videos on page load
}).resize();
  
  
});


/*============================*/
/* Update main product image. */
/*============================*/
var switchImage = function(newImageSrc, newImage, mainImageDomEl) {
  // newImageSrc is the path of the new image in the same size as originalImage is sized.
  // newImage is Shopify's object representation of the new image, with various attributes, such as scr, id, position.
  // mainImageDomEl is the passed domElement, which has not yet been manipulated. Let's manipulate it now.
  jQuery(mainImageDomEl).parents('a').attr('href', newImageSrc.replace('_grande', '_1024x1024'));
  jQuery(mainImageDomEl).attr('src', newImageSrc);  
};

jQuery(function($) {
  
  if($('body#your-shopping-cart').length > 0){
    
    console.log('on the cart');
  	$('.fa-shopping-cart').remove();
  }
   
    
if($('.template-index, #how-it-works, #how-to-use, #about-us').length > 0 && $(window).width() > 991 ){  
 
    
  //sticky nav code
  // Create a clone of the menu, right next to original.
 $('.menu').addClass('original').clone().insertAfter('.menu').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();

scrollIntervalID = setInterval(stickIt, 10);



function stickIt() {

  var orgElementPos = $('.original').offset();
  orgElementTop = orgElementPos.top;               

  if ($(window).scrollTop() >= (orgElementTop)) {
    // scrolled past the original position; now only show the cloned, sticky element.

    // Cloned element should always have same left position and width as original element.     
    orgElement = $('.original');
    coordsOrgElement = orgElement.offset();
    leftOrgElement = coordsOrgElement.left;  
    widthOrgElement = orgElement.css('width');
    $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
    $('.original').css('visibility','hidden');
  } else {
    // not scrolled past the menu; only show the original menu.
    $('.cloned').hide();
    $('.original').css('visibility','visible');
  }
}
}
  //end sticky nav code
  
  
  //smooth scroll
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top - 50
        }, 1000);
        return false;
      }
    }
  });
  
  
  $('.slick-wrap').slick({
    dots: true,
    autoplay: true,
    autoplaySpeed: 30000
  });

  $('.tamper-slick-wrap').slick({
    dots: true,
    autoplay: true,
    autoplaySpeed: 30000
  });
  
  
  //move the buttons on the carousel
 
   $('.slick-wrap .slick-prev').prependTo('.slick-wrap .slick-dots');
   $('.slick-wrap .slick-next').appendTo('.slick-wrap .slick-dots');
  
    $('.tamper-slick-wrap .slick-prev').prependTo('.tamper-slick-wrap .slick-dots');
    $('.tamper-slick-wrap .slick-next').appendTo('.tamper-slick-wrap .slick-dots');
  
                    
  /* Placeholder JS */
  /*==========================*/
  var test = document.createElement('input');
  if (!('placeholder' in test)) {
    $('[placeholder]').each(function(){
      if ($(this).val() === '') {
        var hint = $(this).attr('placeholder');
        $(this).val(hint).addClass('hint');
      }
    });
    $('[placeholder]').focus(function() {
      if ($(this).val() === $(this).attr('placeholder')) {
        $(this).val('').removeClass('hint');
      }
    }).blur(function() {
      if ($(this).val() === '') {
        $(this).val($(this).attr('placeholder')).addClass('hint');
      }
    });    
  }

  /* Form validation JS */
  /*==========================*/

  $('input.error, textarea.error').focus(function() {
    $(this).removeClass('error');
  });

  $('form :submit').click(function() {
    $(this).parents('form').find('input.hint, textarea.hint').each(function() {
      $(this).val('').removeClass('hint');
    });
    return true;
  });
  
  /* Remove SVG images to avoid broken images in all browsers that don't support SVG. */
  /*==========================*/
  
  var supportsSVG = function() {
    return document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#Image', '1.1');
  }  
  if (!supportsSVG()) {
    $('img[src*=".svg"]').remove();
  }
  
  /* Prepare to have floated images fill the width of the design on blog pages on small devices. */
  /*==========================*/ 
    
  var images = $('.article img').load(function() {
    var src = $(this).attr('src').replace(/_grande\.|_large\.|_medium\.|_small\./, '.');
    var width = $(this).width();
    $(this).attr('src', src).attr('width', width).removeAttr('height');
  });
  
  /* Update main product image when a thumbnail is clicked. */
  /*==========================*/
  $('.product-photo-thumb a').on('click', function(e) { 
    e.preventDefault();
    switchImage($(this).attr('href'), null, $('.product-photo-container img')[0]);
  } );
  
  /* == Begin Custom Scripts == */
  
    
  
  
  // Swipebox
  $('.swipebox').swipebox({
    hideBarsDelay : 5000
  });
  
  // Close Swipebox on click anywhere
  $(function(){
    $(document.body)
    .on('click touchend','#swipebox-slider .current img', function(e){
      return false;
    })
    .on('click touchend','#swipebox-slider .current', function(e){
      $('#swipebox-close').trigger('click');
    });
  });
  
  // Flexslider
  $('.flexslider').flexslider();
  
  $('.flexslider-temp').flexslider({
    animation: "slide",
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
  
  $('.middleslider').flexslider({
  		animation: "slide",
        slideshowSpeed: 1000,
        directionNav: false,
        controlNav: true,          
        animationSpeed: 1
  
  });
  
  // Mobile Nav Function
  $(function() {
    $('.toggle-mobile-nav').click(function() {
      toggleMobileNav();
    });
  });
  // Mobile Nav Trigger
  function toggleMobileNav() {
    if ($('body').hasClass('show-mobile-nav')) {
      $('body').removeClass('show-mobile-nav');
    } else {
      $('body').addClass('show-mobile-nav');
    }
  }
  // Mobile Dropdown Toggle
    $(function() {
        $('.toggle-mobile-dropdown').click(function() {
            var $this = $(this).parent('.dropdown');
            if ($this.hasClass('show-child')) {
                $this.removeClass('show-child');
            } else {
                $this.addClass('show-child');
            }
        });
    });
  
});
