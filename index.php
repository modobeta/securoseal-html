<!DOCTYPE html>
<html class="js">
<head>
    <?php include('includes/meta.php'); ?>

    <title>
        Luggage Straps | Security Seals | Tamper Resistant - Securoseal
    </title>

    <?php include('includes/links.php'); ?>
</head>

<body id="luggage-straps-security-seals-tamper-resistant-securoseal" class="template-index FullerSansDT-Regular">
    <header class="site-header header-bg" role="banner">
        <div class="mobile-header visible-xs visible-sm">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <button class="btn btn-link btn-mobile-nav toggle-mobile-nav"><i class="fa fa-bars"></i></button>
                    </div>
                    <div class="col-xs-6">
                        <div class="text-center">
                            <a class="mobile-logo" href="index.php">
                                <img src="dist/img/logo.png" alt="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3"></div>
                </div>
            </div>
        </div>

        <div class="header-top hidden-xs FullerSansDT-Bold hidden-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-lg-2 text-center-sm">
                        <a id="logo" href="https://securoseal.com/">
                            <img src="dist/img/logo.png" alt="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
                        </a>
                    </div>

                    <div class="col-md-10 col-lg-10 text-center-sm">
                        <?php include('includes/menu.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <main class="site-main-content" role="main">
        <div id="top" class="top-slider grey-bg">
            <div id="slick-container" class="container">
                <div class="slick-wrap">
                    <div id="home-slide-1" class="slide-wrap">
                        <div class="slide-text1 col-md-6">
                            <h1 class="Grottel-Light orange-text top100">Know if your luggage<br> has been tampered.</h1>
                            <a href="/pages/why-you-need-seucureoseal" class="orange-text button-img FullerSansDT-Bold">
                                <div class="cf-play">
                                    <img class="bottom" src="dist/img/play_over.png" alt="">
                                    <img class="top" src="dist/img/play.png" alt="">
                                </div>WHY YOU NEED SECUROSEAL
                            </a>
                        </div>
                        <div class="col-md-6"></div>

                        <img src="dist/img/page1_slide1.png" alt="">
                    </div>

                    <div id="home-slide-2" class="slide-wrap">
                        <div class="slide-text1 col-md-6">
                            <h1 class="Grottel-Light orange-text top100">Able to be used</br> in lock-free areas.</h1>
                            <a href="/pages/why-you-need-seucureoseal" class="orange-text button-img FullerSansDT-Bold">
                                <div class="cf-play">
                                    <img class="bottom" src="dist/img/play_over.png" alt="">
                                    <img class="top" src="dist/img/play.png" alt="">
                                </div>WHY YOU NEED SECUROSEAL
                            </a>
                        </div>

                        <div class="col-md-6"></div>

                        <img src="dist/img/page1_slide2.png" alt="">
                    </div>

                    <div id="home-slide-3" class="slide-wrap">
                        <div class="slide-text1 col-md-6">
                            <h1 class="Grottel-Light orange-text top100">Holds 2x check-in weight, lighter than a padlock.</h1>
                            <a href="/pages/why-you-need-seucureoseal" class="orange-text button-img FullerSansDT-Bold">
                                <div class="cf-play">
                                    <img class="bottom" src="dist/img/play_over.png" alt="">
                                    <img class="top" src="dist/img/play.png" alt="">
                                </div>WHY YOU NEED SECUROSEAL
                            </a>
                        </div>

                        <div  class="col-md-6"></div>

                        <img src="dist/img/page1_slide3.png" alt="">
                    </div>

                    <div id="home-slide-4" class="slide-wrap">
                        <div class="slide-text1 col-md-6">
                            <h1 class="Grottel-Light  orange-text top100">Protects your luggage</br> and legal rights.</h1>
                            <a href="/pages/why-you-need-seucureoseal" class="orange-text button-img FullerSansDT-Bold">
                                <div class="cf-play">
                                    <img class="bottom" src="dist/img/play_over.png" alt="">
                                    <img class="top" src="dist/img/play.png" alt="">
                                </div>WHY YOU NEED SECUROSEAL
                            </a>
                        </div>

                        <div  class="col-md-6"></div>

                        <img src=" //cdn.shopify.com/s/files/1/1294/9397/t/3/assets/slide_4_2.png?20532961823290394" alt="">
                    </div>

                    <div id="home-slide-5" class="slide-wrap">
                        <div class="slide-text1 col-md-6">
                            <h1 class="Grottel-Light orange-text top100">Apply in seconds,</br> no tools required.</h1>
                            <a href="/pages/why-you-need-seucureoseal" class="various orange-text button-img FullerSansDT-Bold">
                                <div class="cf-play">
                                    <img class="bottom" src="dist/img/play_over.png" alt="">
                                    <img class="top" src="dist/img/play.png" alt="">
                                </div>WHY YOU NEED SECUROSEAL
                            </a>
                        </div>

                        <div  class="col-md-6"></div>

                        <img src="dist/img/page1_slide5.png" alt="">
                    </div>
                </div>
            </div>

            <div class="page-nav menu original" style="visibility: visible;">
                <div class="container">
                    <div class="col-md-3 hidden-sm hidden-xs">
                        <a href="#logo">
                            <div class="cf-play sub-nav">
                                <img class="bottom" src="dist/img/orange-gear.png" alt="">
                                <img class="top" src="dist/img/grey-gear.png" alt="">
                                <span class="header-link">Why Securoseal?</span>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-12 col-md-9">
                        <div class="sub-page-nav">
                            <a class="hidden-xs" href="#tampering">Tampering</a>
                            <a class="hidden-xs" href="#transport">Transport</a>
                            <a class="hidden-xs" href="#liability">Liability</a>
                            <a class="hidden-xs" href="#security">Security Risk</a>
                            <a class="hidden-xs" href="#tsa">TSA Accepted</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-nav menu cloned">
                <div class="container">
                    <div class="col-md-3 hidden-sm hidden-xs">
                        <a href="#logo">
                            <div class="cf-play sub-nav">
                                <img class="bottom" src="dist/img/orange-gear.png" alt="">
                                <img class="top" src="dist/img/grey-gear.png" alt="">
                                <span class="header-link">Why Securoseal?</span>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-12 col-md-9">
                        <div class="sub-page-nav">
                            <a class="hidden-xs" href="#tampering">Tampering</a>
                            <a class="hidden-xs" href="#transport">Transport</a>
                            <a class="hidden-xs" href="#liability">Liability</a>
                            <a class="hidden-xs" href="#security">Security Risk</a>
                            <a class="hidden-xs" href="#tsa">TSA Accepted</a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tampering" class="tampering dark-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 test">
                            <div class="carousel-homepage middle">
                                <div class="middleslider">
                                    <ul class="slides">
                                        <li>
                                            <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="dist/img/bag1.png" alt=""></a></p>
                                        </li>
                                        <li>
                                            <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="dist/img/bag2.png" alt=""></a></p>
                                        </li>
                                        <li>
                                            <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="dist/img/bag3.png" alt=""></a></p>
                                        </li>
                                        <li>
                                            <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="dist/img/bag4.png" alt=""></a></p>
                                        </li>
                                        <li>
                                            <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="dist/img/bag5.png" alt=""></a></p>
                                        </li>
                                        <li>
                                            <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="dist/img/bag6.png" alt=""></a></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div id="tampering-right" class="col-sm-5  white-text top50 bottom50">
                            <h1 class="Grottel-Light">Tampering happens.</h1>

	                        <p><span class="white-text">All luggage is vulnerable&nbsp;to&nbsp;tampering.</span><br>
		                        Your bag likely features zips or locks. Zips with moving zip sliders can be easily breached and resealed with something as simple as a pen. Most locks, including TSA approved locks, can be opened with a simple paper clip. Without tamper evident seals, you cannot tell whether your bag has been affected. As of September 2015, TSA Locks have been compromised by the release of 3D printer files that allow anyone to print master&nbsp;keys.
                            </p>

                            <p><small>Source: Business Insider &amp; GitHub</small></p>
      
                            <div id="inline" style="display: none;">
                                <h3>Etiam quis mi eu elit</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis mi eu elit tempor facilisis id et neque. Nulla sit amet sem sapien. Vestibulum imperdiet porta ante ac ornare. Nulla et lorem eu nibh adipiscing ultricies nec at lacus. Cras laoreet ultricies sem, at blandit mi eleifend aliquam. Nunc enim ipsum, vehicula non pretium varius, cursus ac tortor. Vivamus fringilla congue laoreet. Quisque ultrices sodales orci, quis rhoncus justo auctor in. Phasellus dui eros, bibendum eu feugiat ornare, faucibus eu mi. Nunc aliquet tempus sem, id aliquam diam varius ac. Maecenas nisl nunc, molestie vitae eleifend vel, iaculis sed magna. Aenean tempus lacus vitae orci posuere porttitor eget non felis. Donec lectus elit, aliquam nec eleifend sit amet, vestibulum sed nunc.</p>
                            </div>
                        </div>
                    </div>
                </div><!--/.container-->
            </div><!--tampering-->
  
            <div id="transport" class="headlines grey-bg thin">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 bottom50">
                            <h1 class="Grottel-Light orange-text">Transport means <br>vulnerability.</h1>
                            <p class="font-pal"><span>The longer the voyage, the greater the risk.</span><br> One flight means two baggage handling systems. With a connection, that becomes three plus storage. By the time your bag is delivered to you, two facts are undeniable: firstly, many people have handled your luggage. Secondly, not all have been supervised. It only takes seconds to breach and reseal unprotected luggage without a trace, leaving you vulnerable to theft and smuggling.</p>
                        </div>

                        <div class="col-sm-6">
                            <div class="blogspecial" style="position: relative;">
                                <!-- START OF LATEST BLOG POSTS -->
                                <div class="widget">
                                    <div class="widget-header">
                                        <h3></h3>
                                    </div>
                                    <div class="latest-blog-posts-wrapper">
                                        <ul class="latest-blog-posts-list">
                                            <li>
                                                <ul>
                                                    <li>
                                                        <time pubdate="" datetime="2016-08-30">Aug 30, 2016</time>
                                                    </li>
                                                </ul>

                                                <div class="art">
                                                  USA: United employee in Denver charged in $129,000...
                                                </div>
                                            </li>

                                            <li>
                                                <ul>
                                                    <li>
                                                        <time pubdate="" datetime="2016-08-29">Aug 29, 2016</time>
                                                    </li>
                                                </ul>

                                                <div class="art">
                                                    Vietnam: Baggage handler admits to stealing tablet at...
                                                </div>
                                            </li>

                                            <li>
                                                <ul>
                                                    <li>
                                                        <time pubdate="" datetime="2016-08-29">Aug 29, 2016</time>
                                                    </li>
                                                </ul>

                                                <div class="art">
                                                  Canada releases advisory on Dominican Republic warning of...
                                                </div>
                                            </li>

                                            <li>
                                                <ul>
                                                    <li>
                                                        <time pubdate="" datetime="2016-08-27">Aug 27, 2016</time>
                                                    </li>
                                                </ul>

                                                <div class="art">
                                                  USA: The last TSA master key has been...
                                                </div>
                                            </li>

                                            <li>
                                                <ul>
                                                    <li>
                                                        <time pubdate="" datetime="2016-08-24">Aug 24, 2016</time>
                                                    </li>
                                                </ul>

                                                <div class="art">
                                                    India: Emirates ordered to repay businessman for credit...
                                                </div>
                                            </li>

                                            <li>
                                                <ul>
                                                    <li>
                                                        <time pubdate="" datetime="2016-08-22">Aug 22, 2016</time>
                                                    </li>
                                                </ul>

                                                <div class="art">
                                                    India: Police break baggage theft ring that ran...
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="liability" class="problem">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7">
                            <img src="dist/img/dog.png" alt="">
                        </div>

                        <div class="col-sm-5">
                            <h1 class="Grottel-Light white-text top40">Your bag.<br>Your problem.</h1>

                            <p class="font-pal"><span class="white-text">Theft can spoil your trip, but smuggling could ruin your life.</span> All travellers are subject to the laws of the countries they visit. When clearing customs, if the luggage is in your possession, most legal systems assume you are the owner of the contents unless you can can prove otherwise. If smuggling has occurred, without tamper evidence like Securoseal, you could find yourself in serious trouble.
                                <a href="https://securoseal.com/pages/usa-new-york-innocent-passenger-used-as-mule-by-baggage-handler-drug-ring" class="various orange-text">It can result in arrest and imprisonment</a>
                            </p>

                            <a href="https://securoseal.com/pages/find-a-real-life-example-here" class="various orange-text button-img FullerSansDT-Bold">
                               <div class="cf-play">
                                    <img class="bottom" src="dist/img/play_over.png" alt="">
                                    <img class="top" src="dist/img/play.png" alt="">
                               </div>

                                FIND A REAL LIFE EXAMPLE HERE
                            </a>

                            <small>Source: Capital News</small>
                        </div>
                    </div>
                </div>
            </div>

            <div id="security" class="bag">
	            <div class="container">
		            <div class="row">
                        <div class="col-sm-6 bottom50">
                            <h1 class="Grottel-Light white-text top50">Every Bag.<br>Everyone's Problem.</h1>
                            <p class="font-pal"><span class="white-text">Lax security makes smuggling guns, bullets and explosives possible.</span> Need examples? In Europe, a customs officer inserted <a target="_blank" href="http://thelede.blogs.nytimes.com/2010/01/05/slovak-air-security-test-goes-very-wrong/">plastic explosives</a> into a passenger’s bag. In USA, a baggage handler smuggled <a target="_blank" href="http://www.reuters.com/article/us-usa-georgia-state-crime-guns-idUSKBN0K11FD20141223#XEHJZR1tCpzLMHPr.97">firearms</a> on commercial flights. In the Philippines, airport staff inserted <a target="_blank" href="http://www.dailymail.co.uk/travel/travel_news/article-3300065/Ninoy-Aquino-International-airport-investigation-security-guards-accused-extorting-passengers-HUNDREDS-pounds-planting-bullets-luggage.html">bullets</a> into luggage for extortion. In Egypt, a concealed <a target="_blank" href="http://www.foxnews.com/world/2015/11/17/russian-security-chief-says-bomb-brought-down-plane-over-sinai.html?intcmp=hplnws">bomb</a> caused a fatal crash. Security starts with you - unprotected baggage is a liability no one can afford.</p>

                            <a href="https://securoseal.com/pages/more-about-bullet-planting-scams" class="orange-text button-img FullerSansDT-Bold">
                                <div class="cf-play">
                                    <img class="bottom" src="dist/img/play_over.png" alt="">
                                    <img class="top" src="dist/img/play.png" alt="">
                                </div>
                                MORE ABOUT BULLET PLANTING SCAMS
                            </a>

                            <small>Source: PTV</small>
                        </div>

                        <div class="col-sm-6">
                            <img id="img-1911" src="dist/img/1911.png" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div id="tsa" class="tsa grey-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7">
                            <img src="dist/img/tsa.png" alt="">
                        </div>

                        <div class="col-sm-5">
                            <h1 class="Grottel-Light orange-text">TSA Accepted.</h1>
                            <p class="font-pal"><span>For use in ‘Lock Free’ zones.</span><br>Only a small percentage of checked-in bags are hand searched by authorities, most are machine scanned. The TSA estimates for every agent that touches a bag, up to ten other people handle the same bag without passenger supervision placing <a href="https://securoseal.com/pages/how-many-bags-does-the-tsa-hand-search">unprotected luggage at risk.</a> Securoseal allows authorities to search if necessary, but unlike a lock you will know if anyone has accessed your luggage – legally or otherwise. Using Securoseal protects you against undetected tampering.</p>

                            <small>Note: TSA makes no claims, warranties or guarantees of any kind, express or implied, as to the goods of any commercial entity, including Securoseal.</small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="chance">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1 class="Grottel-Light">Don't chance it.<br>Get protection.</h1>
                        <p class="font-pal top20">Buy the Securoseal tamper evident luggage seal today.</p>
                    </div>

                    <div id="chance-right" class="col-sm-6">
                        <img id="secure-prod" class="bottom50 hidden-sm" src="dist/img/secure-prod.png" alt="">

                        <div class="desc-img bottom50">
                            <a href="https://securoseal.com/pages/how-it-works">
                                <p class="chance-text FullerSansDT-Bold">HOW IT WORKS</p>

                                <div class="cf">
                                    <img class="bottom" src="dist/img/work-clear.png" alt="">
                                    <img class="top" src="dist/img/work.png" alt="">
                                </div>
                            </a>

                            <a href="https://securoseal.com/pages/how-to-use">
                                <p class="chance-text FullerSansDT-Bold">HOW TO USE</p>
                                <div class="cf">
                                    <img class="bottom" src="dist/img/use-clear.png" alt="">
                                    <img class="top" src="dist/img/use.png" alt="">
                                </div>
                            </a>

                            <a href="https://securoseal.com/pages/about-us">
                                <p class="chance-text FullerSansDT-Bold">ABOUT</p>
                                <div class="cf">
                                    <img class="botom" src="dist/img/about-clear.png" alt="">
                                    <img class="top" src="dist/img/about.png" alt="">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer class="site-footer dark-bg" role="contentinfo">
        <?php include('includes/footer.php'); ?>
    </footer>

    <div class="mobile-nav-wrap hidden-md hidden-lg">
        <a class="mobile-nav-close toggle-mobile-nav" href="#"><i class="fa fa-times"></i></a>

        <?php include('includes/menu-movil.php'); ?>
    </div>

    <?php include('includes/javascript.php'); ?>
</body>
</html>