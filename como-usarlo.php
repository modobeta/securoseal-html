<!DOCTYPE html>
<html class="js">
<head>
    <?php include('includes/meta.php'); ?>

    <title>
        Luggage Straps | Security Seals | Tamper Resistant - Securoseal
    </title>

    <?php include('includes/links.php'); ?>
</head>

<body id="tamper-seal-tamper-proof-tape-luggage-security-securoseal" class="template-page FullerSansDT-Regular" style="">
    <header class="site-header header-bg" role="banner">
        <div class="mobile-header visible-xs visible-sm">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <button class="btn btn-link btn-mobile-nav toggle-mobile-nav"><i class="fa fa-bars"></i></button>
                    </div>

                    <div class="col-xs-6">
                        <div class="text-center">
                            <a class="mobile-logo" href="https://securoseal.com/">
                                <img src="dist/img/logo.png" alt="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
                            </a>
                        </div>
                  </div>

                  <div class="col-xs-3"></div>
                </div>
            </div>
        </div>

        <div class="header-top hidden-xs FullerSansDT-Bold hidden-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-lg-2 text-center-sm">
                        <a id="logo" href="https://securoseal.com/">
                            <img src="dist/img/logo.png" alt="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
                        </a>
                    </div>

                    <div class="col-md-10 col-lg-10 text-center-sm">
                        <?php include('includes/menu.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <main class="site-main-content" role="main">
        <div class="breadcrumbs">
            <div class="container">
                <p>
                    <a href="https://securoseal.com/" class="homepage-link" title="Back to the frontpage">Home</a>
                    <span class="separator">›</span>
                    <span class="page-title">How To Use</span>
                </p>
            </div>
        </div>

        <div id="top" class="top-slider grey-bg how-hero">
            <div class="container">
                <div class="how-hero-text col-sm-5">
                    <h1 class="Grottel-Light orange-text top100">Apply anywhere,<br> anytime in seconds.</h1>
                    <a href="https://securoseal.com/pages/see-how-easy-seucuroseal-is-to-use" class="orange-text button-img FullerSansDT-Bold">
                        <div class="cf-play">
                            <img class="bottom" src="dist/img/play_over.png" alt="">
                            <img class="top" src="dist/img/play.png" alt="">
                        </div>

                        <span>SEE HOW EASY SECUROSEAL IS TO USE</span>
                    </a>
                </div>

                <div class="col-sm-7 how-img-wrapper">
                    <img class="how-img" src="dist/img/how-to-use-hero.png">
                </div>
            </div>
        </div>

        <div class="page-nav menu">
            <div class="container">
                <div class="col-md-3 hidden-sm hidden-xs">
                    <a href="https://securoseal.com/pages/how-to-use#logo">
                        <div class="cf-play sub-nav">
                            <img class="bottom" src="dist/img/orange-gear.png" alt="">
                            <img class="top" src="dist/img/grey-gear.png" alt="">
                            <span class="header-link">How To Use</span>
                        </div>
                    </a>
                </div>

                <div class="col-sm-12 col-md-9">
                    <div class="sub-page-nav">
                        <a href="#instructions">Instructions</a>
                        <a href="#check-in">Check In</a>
                        <a href="#tamper">Tamper Response</a>
                        <a href="#faq">FAQ</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="instructions" class="tampering dark-bg">
            <div id="slick-container" class="container">
                <div class="slick-wrap">
                    <div id="home-slide-1" class="slide-wrap">
                        <div class="slide-text col-md-6">
                            <h1 class="Grottel-Light white-text top50">Instructions for <br>applying Securoseal.</h1>

                            <p><span class="white-text">Apply in 5 easy steps.</span> <br>Every Securoseal comes with pictorial instructions, so using it is easy. It can be applied in seconds with no additional tools.</p>
                        </div>

                        <div class="col-md-6"></div>

                        <img src="dist/img/page3_screen2_panel1.png" alt="">
                    </div>

                    <div id="home-slide-2" class="slide-wrap">
                        <div class="slide-text col-md-6">
                            <h1 class="Grottel-Light white-text top50">Instructions for <br>applying Securoseal.</h1>

                            <img class="instruction-img" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/number-1.png">

                            <p>Remove and retain the <span class="white-text">receipt.</span></p>
                        </div>

                        <div  class="col-md-6"></div>

                        <img src="dist/img/page3_screen2_panel2.png" alt="">
                    </div>

                    <div id="home-slide-3" class="slide-wrap ">
                        <div class="slide-text col-md-6">
                            <h1 class="Grottel-Light white-text top50">Instructions for <br>applying Securoseal.</h1>

                            <img class="instruction-img" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/number-2.png">

                            <p><span class="white-text">Wrap</span> strap around luggage, through handle, through buckle and pull down.</p>
                        </div>

                        <div  class="col-md-6"></div>

                        <img src="dist/img/page3_screen2_panel3.png" alt="">
                    </div>

                    <div id="home-slide-4" class="slide-wrap">
                        <div class="slide-text col-md-6">
                            <h1 class="Grottel-Light white-text top50">Instructions for <br>applying Securoseal.</h1>

                            <img class="instruction-img" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/number-3.png">

                            <p><span class="white-text">Peel &amp; remove </span> backing paper to expose the<br> adhesive strip.</p>
                        </div>

                        <div class="col-md-6"></div>

                        <img src="dist/img/page3_screen2_panel4.png" alt="">
                    </div>

                    <div id="home-slide-5" class="slide-wrap">
                        <div class="slide-text col-md-6">
                            <h1 class="Grottel-Light white-text top50">Instructions for <br>applying Securoseal.</h1>

                            <img class="instruction-img" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/number-4.png">
                            <p><span class="white-text">Affix </span> the adhesive strip to the textured surface of the strap.</p>
                        </div>

                        <div class="col-md-6"></div>

                        <img src="dist/img/page3_screen2_panel5.png" alt="">
                    </div>

                    <div id="home-slide-6" class="slide-wrap">
                        <div class="slide-text col-md-6">
                            <h1 class="Grottel-Light white-text top50">Instructions for <br>applying Securoseal.</h1>

                            <img class="instruction-img" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/number-5.png">

                            <p><span class="white-text">Secure</span> both zip sliders to the strap using the cable tie.</p>
                        </div>

                        <div  class="col-md-6"></div>

                        <img src="dist/img/page3_screen2_panel6.png" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div id="check-in" class="headlines grey-bg thin">
	        <div class="container">
		        <div class="row">
                    <div class="col-sm-5 hidden-xs">
                        <img src="dist/img/page3_screen3.png" alt="">
                    </div>

                    <div class="col-sm-7">
                        <h1 class="Grottel-Light orange-text">Check-in Protocol.</h1>

                        <p class="font-pal"><span>The following steps are recommended at check-in to record the condition of your luggage and minimise your legal risks:</span></p>

                        <ul>
                            <li class="check-in"> <img src="dist/img/lock.png"><p>Apply Securoseal to your luggage.</p></li>
                            <li class="check-in"> <img src="dist/img/camera.png"><p>Photograph your luggage, including the applied seal, it's serial number and the affixed cable tie.</p></li>
                            <li class="check-in"> <img src="dist/img/scroll.png"><p>Ask the check-in operator to affix the tamper evident receipt to your luggage document.</p></li>
                            <li class="check-in"> <img src="dist/img/camera.png"><p>Photograph the check-in operator applying the receipt to the luggage document, including the serial number.</p></li>
                        </ul>
                    </div>

                    <div class="col-sm-5 visible-xs">
                        <img src="dist/img/page3_screen3.png" alt="">
                    </div>
  		        </div>
	        </div>
        </div>

        <div id="tamper" class="problem">
	        <div class="container">
		        <div class="row">
                    <div class="col-sm-6">
                        <h1 class="Grottel-Light white-text top40">Tamper Response <br>Protocol.</h1>

                        <p class="font-pal"><span class="white-text">Here is what to do if tampering has occurred.</span> Each tampering incident is different. The decisions you will need to make are not just determined by the condition of your luggage, but also the laws that apply in the country you are in at the time the incident occurs.</p>

                        <a href="https://securoseal.com/blogs/news/how-to-handle-a-tampering-incident" class="various orange-text button-img FullerSansDT-Bold">
                            <div class="cf-play">
                                <img class="bottom" src="dist/img/button_read_over.png" alt="">
                                <img class="top" src="dist/img/button_read.png" alt="">
                            </div>
                            HOW TO HANDLE A TAMPERING INCIDENT
                        </a>
                    </div>

                    <div class="col-sm-6">
			            <img src="dist/img/page3_screen4.png" alt="">
                    </div>
  		        </div>
	        </div>
        </div>

        <div id="faq" class="faq grey-bg">
	        <div class="container">
		        <div class="row">
                    <div class="col-sm-6 hidden-xs">
                        <img id="brief-case" src="dist/img/page3_screen5.png" alt="">
                    </div>

                    <div class="col-sm-6">
                        <h1 class="Grottel-Light orange-text">FAQ.</h1>

                        <p class="font-pal"><span>Luggage security can be a tricky business. It’s our mission to make it simple for you.</span>We have put together a list of the most commonly asked questions about our product, it’s use and about purchasing from this website. If you’ve got a question, click the link below and see if there is an answer. If you still need additional assistance, our contact details are in there too.</p>

                        <a href="https://securoseal.com/pages/faq" class="orange-text button-img FullerSansDT-Bold">
                            <div class="cf-play">
                                <img class="bottom" src="dist/img/button_read_over.png" alt="">
                                <img class="top" src="dist/img/button_read.png" alt="">
                            </div>
                            FIND ANSWERS TO FREQUENTLY ASKED QUESTIONS
                        </a>
                    </div>

                    <div class="col-sm-6 visible-xs">
			            <img id="brief-case" src="dist/img/page3_screen5.png" alt="">
                    </div>
  		        </div>
	        </div>
        </div>

        <div class="chance">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1 class="Grottel-Light top100">Don't chance it.<br>Get protection.</h1>

                        <p class="font-pal top20">Buy the Securoseal tamper evident luggage seal today.</p>
                    </div>

                    <div id="chance-right" class="col-sm-6">
                        <img id="secure-prod" class="bottom50 hidden-sm hidden-xs" src="dist/img/secure-prod.png" alt="">

                        <div class="desc-img bottom50">
                            <a href="https://securoseal.myshopify.com/">
                                <p class="chance-text FullerSansDT-Bold">WHY SECUROSEAL</p>

                                <div class="cf">
                                    <img class="bottom" src="dist/img/btn_nav_whysecuroseal_over.png" alt="">
                                    <img class="top" src="dist/img/btn_nav_whysecuroseal.png" alt="">
                                </div>
                            </a>

                            <a href="https://securoseal.com/pages/how-it-works">
                                <p class="chance-text FullerSansDT-Bold">HOW IT WORKS</p>

                                <div class="cf">
                                    <img class="bottom" src="dist/img/work-clear.png" alt="">
                                    <img class="top" src="dist/img/work.png" alt="">
                                </div>
                            </a>

                            <a href="https://securoseal.com/pages/about-us">
                                <p class="chance-text FullerSansDT-Bold">ABOUT</p>

                                <div class="cf">
                                    <img class="botom" src="dist/img/about-clear.png" alt="">
                                    <img class="top" src="dist/img/about.png" alt="">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer class="site-footer dark-bg" role="contentinfo">
        <?php include('includes/footer.php'); ?>
    </footer>

    <div class="mobile-nav-wrap hidden-md hidden-lg">
        <a class="mobile-nav-close toggle-mobile-nav" href="#"><i class="fa fa-times"></i></a>

        <?php include('includes/menu-movil.php'); ?>
    </div>

    <?php include('includes/javascript.php'); ?>
</body>
</html>

  

