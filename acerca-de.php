<!DOCTYPE html>
<!-- saved from url=(0037)https://securoseal.com/pages/about-us -->
<html class="js"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' /><![endif]-->

  <title>Tamper Evident | Tamper Proof Bags | Luggage Seal - Securoseal – Luggage Straps | Security Seals | Tamper Resistant - Securoseal</title>

  
  <meta name="description" content="Why did we create Securoseal? To give travelers better tamper evident luggage seals. Find out more about Securoseal here, and how we can help.">
  

  <link rel="canonical" href="https://securoseal.com/pages/about-us">
  
  <link href="./acerca-de_files/css" rel="stylesheet" type="text/css">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  
  
    <meta property="og:image" content="http://cdn.shopify.com/s/files/1/1294/9397/t/3/assets/logo.png?20532961823290394">
    <meta property="og:image:secure_url" content="https://cdn.shopify.com/s/files/1/1294/9397/t/3/assets/logo.png?20532961823290394">
    
	
  <meta property="og:title" content="Tamper Evident | Tamper Proof Bags | Luggage Seal - Securoseal">
  <meta property="og:type" content="website">



<meta property="og:description" content="Why did we create Securoseal? To give travelers better tamper evident luggage seals. Find out more about Securoseal here, and how we can help.">

<meta property="og:url" content="https://securoseal.com/pages/about-us">
<meta property="og:site_name" content="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
  
 


  <meta name="shopify-checkout-api-token" content="f9c8c386a8459164752df9eb2b397489">
<script type="text/javascript" async="" src="./acerca-de_files/jquery.currencies.min.js.descarga"></script><script type="text/javascript" async="" src="./acerca-de_files/1.js.descarga"></script><script type="text/javascript" async="" src="./acerca-de_files/pixel.js.descarga"></script><script type="text/javascript" async="" src="./acerca-de_files/analytics.js.descarga"></script><script src="./acerca-de_files/1761621024095887" async=""></script><script async="" src="./acerca-de_files/fbevents.js.descarga"></script><script type="text/javascript" async="" src="./acerca-de_files/trekkie.storefront.min.js.descarga"></script><script type="text/javascript">
//<![CDATA[
      var Shopify = Shopify || {};
      Shopify.shop = "securoseal.myshopify.com";
      Shopify.theme = {"name":"Madwire Custom Theme","id":110373889,"theme_store_id":null,"role":"main"};
      Shopify.theme.handle = "null";
      Shopify.theme.style = {"id":null,"handle":null};

//]]>
</script><script type="text/javascript">
//<![CDATA[
    (function() {
      function asyncLoad() {
        var urls = ["https:\/\/cdn.refersion.com\/pixel.js?shop=securoseal.myshopify.com\u0026client_id=8604\u0026pk=pub_ec44d8ac130bfeb55ccc\u0026shop=securoseal.myshopify.com","\/\/cdn.ywxi.net\/js\/1.js?shop=securoseal.myshopify.com","\/\/www.mlveda.com\/MultiCurrency\/jquery.currencies.min.js?shop=securoseal.myshopify.com"];
        for (var i = 0; i < urls.length; i++) {
          var s = document.createElement('script');
          s.type = 'text/javascript';
          s.async = true;
          s.src = urls[i];
          var x = document.getElementsByTagName('script')[0];
          x.parentNode.insertBefore(s, x);
        }
      };
      if(window.attachEvent) {
        window.attachEvent('onload', asyncLoad);
      } else {
        window.addEventListener('load', asyncLoad, false);
      }
    })();

//]]>
</script><script id="__st">
//<![CDATA[
var __st={"a":12949397,"offset":-25200,"reqid":"c1b1b582-7a1a-4b96-be43-383404dbab52","pageurl":"securoseal.com\/pages\/about-us","s":"pages-158156545","u":"91d4c1762d32","p":"page","rtyp":"page","rid":158156545};
//]]>
</script><script src="./acerca-de_files/shopify_stats.js.descarga" type="text/javascript" async="async"></script>
<meta id="shopify-digital-wallet" name="shopify-digital-wallet" content="/12949397/digital_wallets/dialog">
<script type="text/javascript">
//<![CDATA[
        window['GoogleAnalyticsObject'] = 'ga';
        window['ga'] = window['ga'] || function() {
          (window['ga'].q = window['ga'].q || []).push(arguments);
        };
        window['ga'].l = 1 * new Date();

//]]>
</script><script src="./acerca-de_files/analytics.js(1).descarga" type="text/javascript" async="async"></script><script type="text/javascript">var _gaUTrackerOptions = {'allowLinker': true};ga('create', 'UA-82818269-1', 'auto', _gaUTrackerOptions);ga('send', 'pageview');
      (function(){
        ga('require', 'linker');
        function addListener(element, type, callback) {
          if (element.addEventListener) {
            element.addEventListener(type, callback);
          }
          else if (element.attachEvent) {
            element.attachEvent('on' + type, callback);
          }
        }
        function decorate(event) {
          event = event || window.event;
          var target = event.target || event.srcElement;
          if (target && (target.action || target.href)) {
            ga(function (tracker) {
              var linkerParam = tracker.get('linkerParam');
              document.cookie = '_shopify_ga=' + linkerParam + '; ' + 'path=/';
            });
          }
        }
        addListener(window, 'load', function(){
          for (var i=0; i<document.forms.length; i++) {
            if(document.forms[i].action && document.forms[i].action.indexOf('/cart') >= 0) {
              addListener(document.forms[i], 'submit', decorate);
            }
          }
          for (var i=0; i<document.links.length; i++) {
            if(document.links[i].href && document.links[i].href.indexOf('/checkout') >= 0) {
              addListener(document.links[i], 'click', decorate);
            }
          }
        })
      }());
    </script><script src="./acerca-de_files/ga_urchin_forms-68ca1924c495cfc55dac65f4853e0c9a395387ffedc8fe58e0f2e677f95d7f23.js.descarga" defer="defer"></script>
      <script type="text/javascript">
        
      window.ShopifyAnalytics = window.ShopifyAnalytics || {};
      window.ShopifyAnalytics.meta = window.ShopifyAnalytics.meta || {};
      window.ShopifyAnalytics.meta.currency = 'USD';
      var meta = {"page":{"pageType":"page","resourceType":"page","resourceId":158156545}};
      for (var attr in meta) {
        window.ShopifyAnalytics.meta[attr] = meta[attr];
      }
    
      </script>

      <script type="text/javascript">
        window.ShopifyAnalytics.merchantGoogleAnalytics = function() {
          
        };
      </script>

      <script type="text/javascript" class="analytics">
        
        

        (function () {
          var customDocumentWrite = function(content) {
            var jquery = null;

            if (window.jQuery) {
              jquery = window.jQuery;
            } else if (window.Checkout && window.Checkout.$) {
              jquery = window.Checkout.$;
            }

            if (jquery) {
              jquery('body').append(content);
            }
          };

          var trekkie = window.ShopifyAnalytics.lib = window.trekkie = window.trekkie || [];
          if (trekkie.integrations) {
            return;
          }
          trekkie.methods = [
            'identify',
            'page',
            'ready',
            'track',
            'trackForm',
            'trackLink'
          ];
          trekkie.factory = function(method) {
            return function() {
              var args = Array.prototype.slice.call(arguments);
              args.unshift(method);
              trekkie.push(args);
              return trekkie;
            };
          };
          for (var i = 0; i < trekkie.methods.length; i++) {
            var key = trekkie.methods[i];
            trekkie[key] = trekkie.factory(key);
          }
          trekkie.load = function(config) {
            trekkie.config = config;
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.onerror = function(e) {
              (new Image()).src = '//v.shopify.com/internal_errors/track?error=trekkie_load';
            };
            script.async = true;
            script.src = 'https://cdn.shopify.com/s/javascripts/tricorder/trekkie.storefront.min.js?v=2017.09.05.1';
            var first = document.getElementsByTagName('script')[0];
            first.parentNode.insertBefore(script, first);
          };
          trekkie.load(
            {"Trekkie":{"appName":"storefront","development":false,"defaultAttributes":{"shopId":12949397,"isMerchantRequest":null,"themeId":110373889,"themeCityHash":15497828523573235020}},"Performance":{"navigationTimingApiMeasurementsEnabled":true,"navigationTimingApiMeasurementsSampleRate":0.1},"Session Attribution":{}}
          );

          var loaded = false;
          trekkie.ready(function() {
            if (loaded) return;
            loaded = true;

            window.ShopifyAnalytics.lib = window.trekkie;
            

            var originalDocumentWrite = document.write;
            document.write = customDocumentWrite;
            try { window.ShopifyAnalytics.merchantGoogleAnalytics.call(this); } catch(error) {};
            document.write = originalDocumentWrite;

            
        window.ShopifyAnalytics.lib.page(
          null,
          {"pageType":"page","resourceType":"page","resourceId":158156545}
        );
      
            
          });

          
      var eventsListenerScript = document.createElement('script');
      eventsListenerScript.async = true;
      eventsListenerScript.src = "//cdn.shopify.com/s/assets/shop_events_listener-4c5801cae3452eff0ededa0ac07d432c1240b78b7e11282cceb3c3213951104b.js";
      document.getElementsByTagName('head')[0].appendChild(eventsListenerScript);
    
        })();
      </script><script async="" src="./acerca-de_files/shop_events_listener-4c5801cae3452eff0ededa0ac07d432c1240b78b7e11282cceb3c3213951104b.js.descarga"></script>
    
<meta id="in-context-paypal-metadata" data-merchant-id="dion@securoseal.com" data-environment="production" data-locale="en_US" data-redirect-url="">
<script src="./acerca-de_files/express_buttons-5fc9e3a0e61068f8c3a7d52556bab8ea9cbcf71ce779e3848ca11d6651e1fbe5.js.descarga" defer="defer" crossorigin="anonymous" integrity="sha256-X8njoOYQaPjDp9UlVrq46py89xzneeOEjKEdZlHh++U="></script><script>
//<![CDATA[
      window.Shopify = window.Shopify || {};
      window.Shopify.Checkout = window.Shopify.Checkout || {};
      window.Shopify.Checkout.apiHost = "securoseal.myshopify.com";
      window.Shopify.Checkout.rememberMeHost = "pay.shopify.com";
      window.Shopify.Checkout.rememberMeAccessToken = "WTFXdzQ5cEFmZU41STNNWTJUeVJsQk9NNW1kcGdSTXUxMWFBd29KQVpIUXRRTDJDcjhRRE8weXZpWVdvbTUzQS0takFGVDVFaHM3TjFJbUNUWjZZcCtiUT09--41a7ee99ba0d728042ad1296b1b950b70c4d08d8";

//]]>
</script>
<script>
//<![CDATA[
window.ShopifyPaypalV4VisibilityTracking = true;
//]]>
</script>

<style media="all">.additional-checkout-button{border:0 !important;border-radius:5px !important;display:inline-block;margin:0 0 10px;padding:0 24px !important;max-width:100%;min-width:150px !important;line-height:44px !important;text-align:center !important}.additional-checkout-button+.additional-checkout-button{margin-left:10px}.additional-checkout-button:last-child{margin-bottom:0}.additional-checkout-button span{font-size:14px !important}.additional-checkout-button img{display:inline-block !important;height:1.3em !important;margin:0 !important;vertical-align:middle !important;width:auto !important}@media (max-width: 500px){.additional-checkout-button{display:block;margin-left:0 !important;padding:0 10px !important;width:100%}}.additional-checkout-button--apple-pay{background-color:#000 !important;color:#fff !important;display:none;font-family:-apple-system, &#39;Helvetica Neue&#39;, sans-serif !important;min-width:150px !important;white-space:nowrap !important}.additional-checkout-button--apple-pay:hover,.additional-checkout-button--apple-pay:active,.additional-checkout-button--apple-pay:visited{color:#fff !important;text-decoration:none !important}.additional-checkout-button--apple-pay .additional-checkout-button__logo{background:-webkit-named-image(apple-pay-logo-white) center center no-repeat !important;background-size:auto 100% !important;display:inline-block !important;vertical-align:middle !important;width:3em !important;height:1.3em !important}@media (max-width: 500px){.additional-checkout-button--apple-pay{display:none}}.additional-checkout-button--paypal-express{background-color:#ffc439 !important}.additional-checkout-button--amazon{background-color:#fad676 !important;position:relative !important}.additional-checkout-button--amazon .additional-checkout-button__logo{-webkit-transform:translateY(4px) !important;transform:translateY(4px) !important}.additional-checkout-button--amazon .alt-payment-list-amazon-button-image{max-height:none !important;opacity:0 !important;position:absolute !important;top:0 !important;left:0 !important;width:100% !important;height:100% !important}.additional-checkout-button-visually-hidden{border:0 !important;clip:rect(0, 0, 0, 0) !important;clip:rect(0 0 0 0) !important;width:1px !important;height:1px !important;margin:-2px !important;overflow:hidden !important;padding:0 !important;position:absolute !important}
</style>

  <link href="./acerca-de_files/style.scss.css" rel="stylesheet" type="text/css" media="all">
  <link href="./acerca-de_files/custom.scss.css" rel="stylesheet" type="text/css" media="all">
  <link href="./acerca-de_files/slick.css" rel="stylesheet" type="text/css" media="all">
  <link href="./acerca-de_files/slick-theme.css" rel="stylesheet" type="text/css" media="all">
  <link href="./acerca-de_files/social-icons.css" rel="stylesheet" type="text/css" media="all">
  <link href="./acerca-de_files/font-awesome.css" rel="stylesheet" type="text/css" media="all">
  <link href="./acerca-de_files/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">
 <link href="./acerca-de_files/mlveda-currencies-switcher-format.css" rel="stylesheet" type="text/css" media="all">

  <link href="./acerca-de_files/css(1)" rel="stylesheet" type="text/css" media="all">
  <link href="./acerca-de_files/MyFontsWebfontsKit.css" rel="stylesheet" type="text/css" media="all">

  <!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
<![endif]-->

  <script src="./acerca-de_files/shopify_common-040322ee69221c50a47032355f2f7e6cbae505567e2157d53dfb0a2e7701839c.js.descarga" type="text/javascript"></script>
  

  <!-- Additional Shopify helpers that will likely be added to the global shopify_common.js some day soon. -->
  <script src="./acerca-de_files/shopify_common.js.descarga" type="text/javascript"></script>
  

  <script src="./acerca-de_files/option_selection-ea4f4a242e299f2227b2b8038152223f741e90780c0c766883939e8902542bda.js.descarga" type="text/javascript"></script>

  <script src="./acerca-de_files/jquery.min.js.descarga" type="text/javascript"></script>
  
  <script>jQuery('html').removeClass('no-js').addClass('js');</script>
  <script src="./acerca-de_files/jquery.fancybox.js.descarga" type="text/javascript"></script>
  <script src="./acerca-de_files/jquery.swipebox.min.js.descarga" type="text/javascript"></script>
  <script src="./acerca-de_files/jquery.flexslider-min.js.descarga" type="text/javascript"></script>
  <script src="./acerca-de_files/shop.js.descarga" type="text/javascript"></script>
  <script src="./acerca-de_files/slick.min.js.descarga" type="text/javascript"></script>
  

<script>window.mlvedaShopCurrency = "USD";
window.shopCurrency = "USD";
window.supported_currencies = "USD EUR AUD CAD CNY GBP HKD INR JPY MXN NZD SGD";
</script>
  
  <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1761621024095887');
fbq('track', "PageView");</script>
<noscript>&lt;img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1761621024095887&amp;ev=PageView&amp;noscript=1"
/&gt;</noscript>
<!-- End Facebook Pixel Code -->
  
<style type="text/css">.fancybox-margin{margin-right:17px;}</style><script type="text/javascript" src="./acerca-de_files/host-loader.js.descarga"></script><script type="text/javascript" src="./acerca-de_files/pub_ec44d8ac130bfeb55ccc.js.descarga"></script><script type="text/javascript" src="./acerca-de_files/host.js.descarga"></script><link type="text/css" rel="stylesheet" charset="UTF-8" href="./acerca-de_files/translateelement.css"></head>

<body id="tamper-evident-tamper-proof-bags-luggage-seal-securoseal" class="template-page FullerSansDT-Regular" style="">

  <header class="site-header header-bg" role="banner">

    

    <div class="mobile-header visible-xs visible-sm">
      <div class="container">
        <div class="row">
          <div class="col-xs-3">
            <button class="btn btn-link btn-mobile-nav toggle-mobile-nav"><i class="fa fa-bars"></i></button>
          </div>
          <div class="col-xs-6">
<!--             <div class="mobileCart-link text-right">
              <a class="btn btn-link btn-mobileCart" href="/cart"><i class="fa fa-shopping-cart"></i> 0 (<span class=money>$0.00 USD</span>)</a>
            </div> -->
            <div class="text-center">
            <a class="mobile-logo" href="https://securoseal.com/">
              <img src="./acerca-de_files/logo.png" alt="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
            </a>
            </div>
          </div>
          <div class="col-xs-3"></div>
        </div>
      </div>
    </div>
    
    <!--<div class="header-main">
      <div class="container">
        

        <div class="row">
          <div class="col-md-8 col-lg-9 text-center-sm">
            
          </div>
          <div class="col-md-4 col-lg-3 text-right hidden-xs hidden-sm">
            
            <div class="customer-links text-right">
              
              <a href="/account/login" id="customer_login_link">Log in</a>
              
              
            </div>
            
          </div>
        </div>
      </div>
    </div>-->

    <div class="header-top hidden-xs FullerSansDT-Bold hidden-sm">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-lg-2 text-center-sm">
            
            <a id="logo" href="https://securoseal.com/">
              <img src="./acerca-de_files/logo.png" alt="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
            </a>
            
          </div>
          <div class="col-md-10 col-lg-10 text-center-sm">
           
            <div class="logon">
            
            <div class="customer-links text-right">
              
              <a href="https://securoseal.com/account/login" id="customer_login_link">Log in</a>
              
              
            </div>
            
            </div>
             

<div class="pick_currency" align="right" style=""><label for="currencies" style="display: inline;">Pick a currency </label>
<select id="currencies" name="currencies" style="display: inline;">
  
  
  <option value="USD" selected="selected">USD</option>
  
    
  
    
    <option value="EUR">EUR</option>
    
  
    
    <option value="AUD">AUD</option>
    
  
    
    <option value="CAD">CAD</option>
    
  
    
    <option value="CNY">CNY</option>
    
  
    
    <option value="GBP">GBP</option>
    
  
    
    <option value="HKD">HKD</option>
    
  
    
    <option value="INR">INR</option>
    
  
    
    <option value="JPY">JPY</option>
    
  
    
    <option value="MXN">MXN</option>
    
  
    
    <option value="NZD">NZD</option>
    
  
    
    <option value="SGD">SGD</option>
    
  
</select></div>


        

<nav role="navigation">
  <ul class="main-nav topper clearfix">
    
    
    
    
    
    
    <li class=" first">
      <a href="https://securoseal.com/">Why Securoseal?</a> 
      
    </li>
    
    
    
    
    
    
    <li class="">
      <a href="https://securoseal.com/pages/how-it-works">How It Works</a> 
      
    </li>
    
    
    
    
    
    
    <li class="">
      <a href="https://securoseal.com/pages/how-to-use">How To Use</a> 
      
    </li>
    
    
    
    
    
    
    <li class=" active">
      <a href="https://securoseal.com/pages/about-us">About Us</a> 
      
    </li>
    
    
    
    
    
    
    <li class="">
      <a href="https://securoseal.com/blogs/news">News</a> 
      
    </li>
    
    
    
    
    
    
    <li class=" last">
      <a href="https://securoseal.com/collections/all">Store</a> 
      
    </li>
    
  </ul>
</nav>

            
          </div>
        </div>
      </div>
    </div>
    
<!-- Begin slider -->
<!--<div class="carousel-homepage parallax">
  <div class="flexslider">
    <ul class="slides">
      { % for i in (1..4) %}
      { % capture show_slide %}slide_{ { i }}{ % endcapture %}
      { % capture image %}slideshow_{ { i }}.png{ % endcapture %}
      { % capture heading %}slide_{ { i }}_heading{ % endcapture %}
      { % capture content %}slide_{ { i }}_content{ % endcapture %}
      { % capture button_text %}slide_{ { i }}_btn{ % endcapture %}
      { % capture link %}slide_{ { i }}_link{ % endcapture %}
      { % capture alt %}slide_{ { i }}_alt{ % endcapture %}
      { % if settings[show_slide] %}
      <li>
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <h2>{ { settings[heading] }}</h2>
              <p class="font-lg"><a class="stuff" href="{ { settings[link] }}"><img src="{ { 'button_play.png' | asset_url }}" alt=""></a>
              { { settings[content] }}</p>
              
            </div>
            
            <div class="col-sm-6">
              <p class="text-center"><img src="{ { image | asset_url }}" alt="{ { settings[alt] | escape }}" /></p>
            </div>
          </div>
        </div>
      </li>
      { % endif %}
      { % endfor %}
    </ul>
    <div class="flex-controls"></div>
  </div>
</div>
<!-- End slider -->

    
   

<!--     <div class="site-header-nav hidden-xs hidden-sm">
      <div class="container">
        { % include 'site-nav' %}
      </div>
    </div> -->

  </header>

  <main class="site-main-content" role="main">
    
<div class="breadcrumbs">
  <div class="container">
    <p>
      <a href="https://securoseal.com/" class="homepage-link" title="Back to the frontpage">Home</a>
      
         <span class="separator">›</span>
         <span class="page-title">About us</span>
      
    </p>
  </div>
</div>

    
    




<div id="top" class="top-slider grey-bg">
  <div id="slick-container" class="container">
    
      <div class="slick-wrap slick-initialized slick-slider">
       <!-- <div id="home-slide-1" class="slide-wrap">
          <div class="slide-text col-md-6">
            <h1 class="Grottel-Light orange-text top100">Know if your luggage<br> has been tampered.</h1>
             <a href="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/test.html?20532961823290394" data-fancybox-type="iframe" class="various orange-text button-img FullerSansDT-Bold">
          	  <div class="cf-play">
                	<img class="bottom" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/play_over.png?20532961823290394" alt="">
                	<img class="top" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/play.png?20532961823290394" alt="">
              </div>WHY YOU NEED SECUROSEAL</a>
          </div>
          <div class="col-md-6">
          </div>
        <img src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/page1_slide1.png?20532961823290394" alt="">
        </div>-->
        <div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 970px; transform: translate3d(0px, 0px, 0px);"><div id="home-slide-2" class="slide-wrap slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 970px;">
          <div class="slide-text col-md-5">
            <h1 class="Grottel-Light orange-text top100">We are passengers,<br> just like you.</h1>
            <p class="Grottel-Light orange-text">
              <b>When we discovered that existing products failed to protect us, we decided to protect ourselves.</b></p>
          	<!--<img class="col-sm-6" src=" { {'page1_screen1_panel2a.png' | asset_url }}" alt="">-->
          </div>
          <div class="col-md-7">	
        </div>
            <img src="./acerca-de_files/aboutussliders.png" alt="">
         
          
        </div></div></div>
       <!-- <div id="home-slide-3" class="slide-wrap ">
          <div class="slide-text col-md-6">
            <h1 class="Grottel-Light orange-text top100">Holds 2x check-in weight, lighter than a padlock.</h1>
            <a href="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/test.html?20532961823290394" data-fancybox-type="iframe" class="various orange-text button-img FullerSansDT-Bold">
          	  <div class="cf-play">
                	<img class="bottom" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/play_over.png?20532961823290394" alt="">
                	<img class="top" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/play.png?20532961823290394" alt="">
              </div>WHY YOU NEED SECUROSEAL</a>
          </div>
          <div  class="col-md-6">   
          </div>
          
          <img src=" //cdn.shopify.com/s/files/1/1294/9397/t/3/assets/page1_slide3.png?20532961823290394" alt="">
          
        </div>-->
        <!--<div id="home-slide-4" class="slide-wrap">
          <div class="slide-text col-md-6">
            <h1 class="Grottel-Light  orange-text top100">Protects your luggage</br> and legal rights.</h1>
            <a href="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/test.html?20532961823290394" data-fancybox-type="iframe" class="various orange-text button-img FullerSansDT-Bold">
          	  <div class="cf-play">
                	<img class="bottom" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/play_over.png?20532961823290394" alt="">
                	<img class="top" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/play.png?20532961823290394" alt="">
              </div>WHY YOU NEED SECUROSEAL</a>
          </div>
  			<div  class="col-md-6">
  				
  			</div>
  
         <img src=" //cdn.shopify.com/s/files/1/1294/9397/t/3/assets/page1_slide4.png?20532961823290394" alt="">
            
        </div>
        <div id="home-slide-5" class="slide-wrap">
          
          
          
          <div class="slide-text col-md-6">
            <h1 class="Grottel-Light orange-text top100">Apply in seconds,</br> no tools required.</h1>
            <a href="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/test.html?20532961823290394" data-fancybox-type="iframe" class="various orange-text button-img FullerSansDT-Bold">
          	  <div class="cf-play">
                	<img class="bottom" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/play_over.png?20532961823290394" alt="">
                	<img class="top" src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/play.png?20532961823290394" alt="">
              </div>WHY YOU NEED SECUROSEAL</a>
          </div>
		  <div  class="col-md-6">
			
		  </div>

		  <img src=" //cdn.shopify.com/s/files/1/1294/9397/t/3/assets/page1_slide5.png?20532961823290394" alt="">

        </div>-->
      </div>

  </div>
</div>

<div class="page-nav menu">
  <div class="container">
  <div class="col-md-3 hidden-sm hidden-xs">
    <a href="https://securoseal.com/pages/about-us#logo">
      <div class="cf-play sub-nav">
                	<img class="bottom" src="./acerca-de_files/orange-gear.png" alt="">
                	<img class="top" src="./acerca-de_files/grey-gear.png" alt="">
        <span class="header-link">About Us</span>
      </div>
      
    </a>
  </div>
  <div class="col-sm-12 col-md-9">
    <div class="sub-page-nav">
      <a href="https://securoseal.com/pages/about-us#story">Our Story</a>
      <a href="https://securoseal.com/pages/about-us#company">Our Company</a>
      <a href="https://securoseal.com/pages/about-us#management">Management</a>
      <a href="https://securoseal.com/pages/about-us#contact">Contact</a>
      <a class="orange-text orange-hollow" href="https://securoseal.com/collections/all"><img class="nav-prod" src="./acerca-de_files/sub-nav-prod.png" alt=""> BUY NOW</a>
    </div>
  </div>
  </div>
</div>

<div id="tampering" class="tampering dark-bg">

<div class="container">
  
  <div class="row">
    <div class="col-sm-7">
      <!-- Begin slider -->
<div class="carousel-homepage middle">
  <div class="middleslider">
    
  <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 600%; transition-duration: 0s; transform: translate3d(-536px, 0px, 0px);"><li class="clone" aria-hidden="true" style="float: left; display: block; width: 536px;">
        <p class="" style="margin-bottom: 0;"><a class="middleslider" href="https://securoseal.com/pages/about-us#"><img src="./acerca-de_files/globe.png" alt="" draggable="false"></a></p>  
      </li>
      <li class="flex-active-slide" style="float: left; display: block; width: 536px;">
        <p class="" style="margin-bottom: 0;"><a class="middleslider" href="https://securoseal.com/pages/about-us#"><img src="./acerca-de_files/globe.png" alt="" draggable="false"></a></p>  
      </li>
     <!-- <li>
        <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/bag2.png?20532961823290394" alt=""></a></p>  
      </li>
      <li>
        <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/bag3.png?20532961823290394" alt=""></a></p>  
      </li>
      <li>
        <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/bag4.png?20532961823290394" alt=""></a></p>  
      </li>
      <li>
        <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/bag5.png?20532961823290394" alt=""></a></p>  
      </li>
      <li>
        <p class="" style="margin-bottom: 0;"><a class="middleslider" href="#"><img src="//cdn.shopify.com/s/files/1/1294/9397/t/3/assets/bag6.png?20532961823290394" alt=""></a></p>  
      </li>-->
    <li class="clone" aria-hidden="true" style="float: left; display: block; width: 536px;">
        <p class="" style="margin-bottom: 0;"><a class="middleslider" href="https://securoseal.com/pages/about-us#"><img src="./acerca-de_files/globe.png" alt="" draggable="false"></a></p>  
      </li></ul></div><ol class="flex-control-nav flex-control-paging"></ol></div>
</div>
<!-- End slider -->
    </div>
    <div id="story" class="col-sm-5  white-text top40 bottom50">
      <h1 class="Grottel-Light">Our Story.</h1>
	<p>Securoseal was commissioned in 2005, after a crisis in travel security was discovered in Australia by the <a href="https://securoseal.com/pages/wheeler-inquiry-into-airport-security-for-the-government-of-australia">Wheeler Inquiry</a> involving widespread airport and luggage crime.<br>
<br>
      After international systems tests in various airports around the world, the Securoseal product was first offered to the public in 2010. The launch coincided with a successful <a href="https://securoseal.com/pages/south-africa-securoseal-product-foils-baggage-thieves">operation</a> launched with Airports Company South Africa to safeguard flights from Johannesburg against baggage tampering immediately prior to FIFA World Cup.</p>
		<!--<div class="play-links"><a target="_self" href="https://www.securoseal.com/main.php?id=14113&amp;youtube=UF7edD-pF_w" class="lightbox cboxElement"><img src="https://www.securoseal.com/V2/images/global/blank.png" height="48px" width="48px"></a><a target="_self" href="https://www.securoseal.com/main.php?id=14113&amp;youtube=UF7edD-pF_w" class="lightbox cboxElement">
			<p><a class="stuff" href="{ { settings[link] }}"><img src="{ { 'button_play.png' | asset_url }}" alt=""></a>&nbsp; &nbsp; &nbsp; <a class="stuff" href="">How the TSA key leak affects you</a></p>
		</a></div>-->
  
      
    </div>
  </div>
  
  

  
  
  
</div><!--/.container-->

</div><!--tampering-->
  
  <div id="company" class="headlines grey-bg thin">  
	<div class="container">
		<div class="row">
          <div class="col-sm-6 bottom50">
            <h1 class="Grottel-Light orange-text">Our Company <br>&amp; Product.</h1>
            <p class="font-pal">Securoseal is an internationally patented product. It was designed in Australia by a team of qualified industrial design, materials and adhesives engineers. It has been tested and refined in different operational environments to ensure reliable performance.<br><br>
              Securoseal Global Ltd (a Hong Kong company) is responsible for the manufacture, sale and marketing of the Securoseal product. It has offices in Australia, Hong Kong, China, Colombia and the USA and a number of representative companies in other regions. Securoseal is affiliated with <a href="http://www.looop.com.au/">Looop</a>, an international private equity and investment group.</p>
          </div>
          <div class="col-sm-6">
			<img src="./acerca-de_files/world.png" alt="">
          </div>
  		</div>
	</div>
</div>
<div id="management" class="people">  
	<div class="container">
		<div class="row">
          <div class="col-sm-1"></div>
          <div class="col-sm-10 bottom50">
            <h1 class="Grottel-Light top40 bottom40">Our Management</h1>
        	<div class="row bottom20">
          		<div class="col-sm-2">
              <img src="./acerca-de_files/photo-jeff.png" alt="">
              </div>
         		 <div class="col-sm-10">
                   <h4>Mr Jeff Friedmann / Director -</h4>
                   <p>Jeff Friedmann is CEO of Looop, a private equity venture capital group which operates numerous successful business enterprises internationally.</p>
                   <p><a href="https://securoseal.myshopify.com/pages/mr-jeff-friedmann">More about Jeff.</a></p>
              </div>
           </div>
            <div class="row bottom20">
          		<div class="col-sm-2">
              <img src="./acerca-de_files/photo-daniel.png" alt="">
              </div>
         		 <div class="col-sm-10">
                   <h4>Dr Daniel Levin / Director -</h4>
                   <p>Dr. Daniel Levin is a Board member of Liechtenstein Foundation for State Governance and several leading financial institutions.</p>
                   <p><a href="https://securoseal.myshopify.com/pages/dr-daniel-levin">More about Daniel.</a></p>
              </div>
           </div>
            <div class="row bottom20">
				<div class="col-sm-2">
					<img src="./acerca-de_files/photo-dion.png" alt="">
				</div>
				<div class="col-sm-10">
					<h4>Mr Dion Mrocki / Director - CEO Securoseal</h4>
					<p>Dion Mrocki is General Counsel at Looop and founder of Securoseal Pty Ltd, the entity responsible for research, development and testing of the Securoseal product.</p>
					<p><a href="https://securoseal.myshopify.com/pages/mr-dion-mrocki">More about Dion.</a></p>
				</div>
			</div>
             <div class="row bottom20">
				<div class="col-sm-2">
					<img src="./acerca-de_files/photo-enrique.png" alt="">
				</div>
				<div class="col-sm-10">
					<h4>Mr Enrique Venguer / Managing Director, Latin America -</h4>
					<p>Enrique Venguer is an international transactions expert and leads our Latin American division. He is also affiliated with WW Capital Partners AG.</p>
					<p><a href="https://securoseal.myshopify.com/pages/mr-enrique-venguer">More about Enrique.</a></p>
				</div>
			</div>
             <div class="row bottom20">
				<div class="col-sm-2">
					<img src="./acerca-de_files/photo-flor.png" alt="">
				</div>
				<div class="col-sm-10">
					<h4>Ms Flor Marina Yepes Vargas / Managing Director, Securoseal de Colombia SAS -</h4>
					<p>Flor Marina Yepes Vargas is responsible for the management of Securoseal de Colombia SAS, our Colombian subsidiary. She is also affiliated with Basic Farm LTDA. </p>
					<p><a href="https://securoseal.myshopify.com/pages/ms-flor-marina-yepes-vargas">More about Flor Marina.</a></p>
				</div>
			</div>
          </div>
          <div class="col-sm-1"></div>
  		</div>
	</div>
</div>
<div id="contact" class="tsa grey-bg new">  
	<div class="container">
		<div class="row">
          <div class="col-sm-6">
            <h1 class="Grottel-Light orange-text">Contact Us.</h1>
            <p class="font-pal"><span>Securoseal welcomes your inquires.</span></p>
            <ul>
              <li>General Inquiries:<a href="mailto:info@securoseal.com"> info@securoseal.com</a></li>
              <li>Orders in Progress:<a href="mailto:shipping@securoseal.com"> shipping@securoseal.com</a></li>
              <li>Trade Inquiry (USA):<a href="mailto:americas@securoseal.com"> americas@securoseal.com</a></li>
              <li>Trade Inquiry (Asia Pacific):<a href="mailto:asiapacific@securoseal.com"> asiapacific@securoseal.com</a></li>
              <li>Trade Inquiry (EU, ME &amp; Africa):<a href="mailto:emea@securoseal.com"> emea@securoseal.com</a></li>
              <li>Trade Inquiry (Government):<a href="mailto:govtrade@securoseal.com"> govtrade@securoseal.com</a></li>
            </ul>
            <p>Securoseal thanks you for your interest.<br>
			We will respond to your inquiry at our earliest opportunity.</p>
            <h4>Securoseal</h4>
<p>1801 Century Park East, Suite 1560<br>
Los Angeles, CA, 90067<br>
(213) 604 6876</p>
            
          </div>
          <div class="col-sm-6">
            <img src="./acerca-de_files/plane.png" alt="">
          </div>
  		</div>
	</div>
</div>
<div class="chance">  
	<div class="container">
		<div class="row">
          <div class="col-sm-6">
            <h1 class="Grottel-Light top100">Don't chance it.<br>Get protection.</h1>
            <p class="font-pal top20">Buy the Securoseal tamper evident luggage seal today.
            </p>
          <a href="https://securoseal.com/collections/all" class="orange-text orange-hollow top10">BUY NOW</a>
          </div>
          <div id="chance-right" class="col-sm-6">
			<img id="secure-prod" class="bottom50 hidden-sm hidden-xs" src="./acerca-de_files/secure-prod.png" alt="">
            <div class="desc-img bottom50">
              <a href="https://securoseal.myshopify.com/">
                <p class="chance-text FullerSansDT-Bold">WHY SECUROSEAL</p>
                <div class="cf">
                	<img class="botom" src="./acerca-de_files/btn_nav_whysecuroseal_over.png" alt="">
                	<img class="top" src="./acerca-de_files/btn_nav_whysecuroseal.png" alt="">
                </div>
              </a>
              <a href="https://securoseal.com/pages/how-it-works">
                <p class="chance-text FullerSansDT-Bold">HOW IT WORKS</p>
                <div class="cf">
                	<img class="bottom" src="./acerca-de_files/work-clear.png" alt="">
                	<img class="top" src="./acerca-de_files/work.png" alt="">
                </div>
              </a>
              <a href="https://securoseal.com/pages/how-to-use">
                <p class="chance-text FullerSansDT-Bold">HOW TO USE</p>
                <div class="cf">
                	<img class="bottom" src="./acerca-de_files/use-clear.png" alt="">
                	<img class="top" src="./acerca-de_files/use.png" alt="">
                </div>
              </a>
            </div>
          </div>
  		</div>
	</div>
</div>
  </main>

  

  <footer class="site-footer dark-bg" role="contentinfo">
    <div class="container">
      
      
      <div class="row footer-row1">
        <div class="col-sm-6">
          <a href="https://securoseal.com/#logo"><div class="cf-play">
                  <img class="bottom" src="./acerca-de_files/large-orange-gear.png" alt="">
                  <img class="top" src="./acerca-de_files/large-white-gear.png" alt="">
            </div></a>
          <p class="white-text">SECUROSEAL - TAMPER EVIDENT LUGGAGE SEAL</p>
        </div>
        <div class="col-sm-6 text-right">
          <ul class="social-m">
            <li><a href="https://www.facebook.com/Securoseal-1545230415730067/">
              <div class="cf-play">
                  <img class="bottom" src="./acerca-de_files/social_facebook_over.png" alt="">
                  <img class="top" src="./acerca-de_files/social_facebook.png" alt="">
              </div></a>
            </li>
            <li><a href="https://plus.google.com/116459602198520644377/posts">
              <div class="cf-play">
                  <img class="bottom" src="./acerca-de_files/social_googleplus_over.png" alt="">
                  <img class="top" src="./acerca-de_files/social_googleplus.png" alt="">
          	  </div></a>
            </li>
            <li><a href="https://twitter.com/securoseal">
              <div class="cf-play">
                  <img class="bottom" src="./acerca-de_files/social_twitter_over.png" alt="">
                  <img class="top" src="./acerca-de_files/social_twitter.png" alt="">
          	  </div></a>
            </li>
            <li><a href="https://www.youtube.com/user/securoseal">
              <div class="cf-play">
                  <img class="bottom" src="./acerca-de_files/social_youtube_over.png" alt="">
                  <img class="top" src="./acerca-de_files/social_youtube.png" alt="">
          	  </div></a>
            </li>
          </ul>
        </div>
      </div>
      <div class="row footer-row2">
      	<div class="col-sm-6">
          
          <small>Copyright © 2009-2016 Securoseal. All Rights Reserved.</small>
        </div>
        <div class="col-sm-6 text-right">
          <ul class="footer-links">
            <li><a href="https://securoseal.com/pages/privacy-policy">Privacy</a></li>
            <li><a href="https://securoseal.com/pages/terms-of-use">Terms of Use</a></li>
          </ul>
        </div>
      </div>
      <div class="row footer-row3">
      	<div class="col-sm-6">
          <ul class="seals">
            <li><script src="./acerca-de_files/inline.js.descarga"></script><a target="_blank" href="https://www.mcafeesecure.com/verify?host=securoseal.com"><img class="mfes-trustmark mfes-trustmark-hover" border="0" src="./acerca-de_files/102.gif" width="105" height="43" title="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="window.open(&#39;https://www.mcafeesecure.com/verify?host=securoseal.com&#39;); return false;"></a></li>
            <li><a href="https://www.shopify.ca/pci-compliant?utm_source=secure&amp;utm_medium=shop" title="This online store is secured by Shopify" target="_blank"><img src="./acerca-de_files/shopify-secure-badge-light-shadow.png" alt="Shopify secure badge"></a></li>
            <!--<li><img class="top" src="{ { 'www.securoseal.png' | asset_url }}" alt=""></li> -->
            <li><img class="top" src="./acerca-de_files/logo-paypal-verified.png" alt=""></li>
          </ul>
        </div>
        <div class="col-sm-6 text-right">
        </div>
      </div>
      
      
    </div><!--/.container-->
     <!--/.footer-bottom-->
  </footer><!--/.site-footer-->

  <div class="mobile-nav-wrap hidden-md hidden-lg">
  <a class="mobile-nav-close toggle-mobile-nav" href="https://securoseal.com/pages/about-us#"><i class="fa fa-times"></i></a>
  <nav role="navigation">
    <h3 class="mobile-nav-heading">Categories</h3>
    <ul class="mobile-nav">
      
      
      
      
      <li><a href="https://securoseal.com/collections/all" title="">Shop All</a></li>
      
      
    </ul>
    
    <h3 class="mobile-nav-heading">Pages</h3>
    <ul class="mobile-nav">
      
      
      
      
      
      
      <li class=" first">
        <a href="https://securoseal.com/">Why Securoseal?</a>
        
        
      </li>
      
      
      
      
      
      
      <li class="">
        <a href="https://securoseal.com/pages/how-it-works">How It Works</a>
        
        
      </li>
      
      
      
      
      
      
      <li class="">
        <a href="https://securoseal.com/pages/how-to-use">How To Use</a>
        
        
      </li>
      
      
      
      
      
      
      <li class=" active">
        <a href="https://securoseal.com/pages/about-us">About Us</a>
        
        
      </li>
      
      
      
      
      
      
      <li class="">
        <a href="https://securoseal.com/blogs/news">News</a>
        
        
      </li>
      
      
      
      
      
      
      <li class=" last">
        <a href="https://securoseal.com/collections/all">Store</a>
        
        
      </li>
      
      <li>

<div class="pick_currency" align="right" style=""><label for="currencies" style="display: inline;">Pick a currency </label>
<select id="currencies" name="currencies" style="display: inline;">
  
  
  <option value="USD" selected="selected">USD</option>
  
    
  
    
    <option value="EUR">EUR</option>
    
  
    
    <option value="AUD">AUD</option>
    
  
    
    <option value="CAD">CAD</option>
    
  
    
    <option value="CNY">CNY</option>
    
  
    
    <option value="GBP">GBP</option>
    
  
    
    <option value="HKD">HKD</option>
    
  
    
    <option value="INR">INR</option>
    
  
    
    <option value="JPY">JPY</option>
    
  
    
    <option value="MXN">MXN</option>
    
  
    
    <option value="NZD">NZD</option>
    
  
    
    <option value="SGD">SGD</option>
    
  
</select></div>

</li>
    </ul>
    <h3 class="mobile-nav-heading">Account</h3>
    <ul class="mobile-nav">
      
      
      <li><a href="https://securoseal.com/account/login" id="customer_login_link">Log in</a></li>
      
      <li><a href="https://securoseal.com/account/register" id="customer_register_link">Create an account</a></li>
      
      
      
      <li><a href="https://securoseal.com/cart">View Cart</a></li>
    </ul>
  </nav>
</div>

  
  <!--[if lt IE 8]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->



<script src="./acerca-de_files/currencies.js.descarga" type="text/javascript"></script>

<script>
function mlvedaCreateCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function mlvedaReadCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function mlvedaEraseCookie(name) {
    createCookie(name, "", -1);
}

function roundCurrency() {
  
    
      $('.money').each(function(){
        if (Currency.currentCurrency === "JOD" || Currency.currentCurrency === "KWD" || Currency.currentCurrency === "BHD") {
          var moneyraw = parseFloat(jQuery(this).html().replace(/[^0-9]/g, ""), 10) / 10 ;
        } 
        else if( Currency.currentCurrency === "KRW" || Currency.currentCurrency === "JPY" || Currency.currentCurrency === "SEK" || Currency.currentCurrency === "HUF" || Currency.currentCurrency === "ISK") {
          var moneyraw = parseFloat(jQuery(this).html().replace(/[^0-9]/g, ""), 10) * 100 ;
        } else {
          var moneyraw = parseFloat(jQuery(this).html().replace(/[^0-9]/g, ""), 10)  ;
        }  
        var moneyrounded = Math.ceil(moneyraw / 5) * 5;
        if(Currency.format === "money_with_currency_format") {
          var acuree = Currency.formatMoney(moneyrounded, Currency.moneyFormats[Currency.currentCurrency].money_with_currency_format)
        }
        else {
          var acuree = Currency.formatMoney(moneyrounded, Currency.moneyFormats[Currency.currentCurrency].money_format)
        }        jQuery(this).html(acuree);
      });
    
  
}



function mlvedaload(){


Currency.format = 'money_with_currency_format';


var shopCurrency = 'USD';

/* Sometimes merchants change their shop currency, let's tell our JavaScript file */
Currency.moneyFormats[shopCurrency].money_with_currency_format = "${{amount}} USD";
Currency.moneyFormats[shopCurrency].money_format = "${{amount}} USD";
  


/* Default currency */
var defaultCurrency = shopCurrency;
  
/* Cookie currency */
var cookieCurrency = Currency.cookie.read();
var resetCurrencyCookie = mlvedaReadCookie("resetCurrencyCookie");
if (resetCurrencyCookie == null)
{
    cookieCurrency = null;
    mlvedaCreateCookie("resetCurrencyCookie", 1, 365);
}

  if(cookieCurrency == null) {
    cookieCurrency = window.mlvedadefaultcurrency;
  }
/* Fix for customer account pages */
jQuery('span.money span.money').each(function() {
  jQuery(this).parents('span.money').removeClass('money');
});

/* Saving the current price */
jQuery('span.money').each(function() {
if(!jQuery(this).attr('data-currency-USD'))
{
  jQuery(this).attr('data-currency-USD', jQuery(this).html());
}
});

// If there's no cookie.
if (cookieCurrency == null) {
  if (shopCurrency !== defaultCurrency) {
    Currency.convertAll(shopCurrency, defaultCurrency);
  }
  else {
    Currency.currentCurrency = defaultCurrency;
  }
}
// If the cookie value does not correspond to any value in the currency dropdown.
else if (jQuery('[name=currencies]').size() && jQuery('[name=currencies] option[value=' + cookieCurrency + ']').size() === 0) {
  Currency.currentCurrency = shopCurrency;
  Currency.cookie.write(shopCurrency);
}
else if (cookieCurrency === shopCurrency) {
  Currency.currentCurrency = shopCurrency;
}
else {
  Currency.convertAll(shopCurrency, cookieCurrency);
  jQuery(".mlvedaCartNote").css("display","inline");
  roundCurrency();
}

jQuery('[name=currencies]').val(Currency.currentCurrency).change(function() {
  var newCurrency = jQuery(this).val();
  Currency.convertAll(Currency.currentCurrency, newCurrency);
  jQuery('.selected-currency').text(Currency.currentCurrency);
  if(newCurrency != shopCurrency) {
    roundCurrency();
    jQuery(".mlvedaCartNote").css("display","inline");
  } else {
    jQuery(".mlvedaCartNote").css("display","none");
  }});

var original_selectCallback = window.selectCallback;
var selectCallback = function(variant, selector) {
  original_selectCallback(variant, selector);
  Currency.convertAll(shopCurrency, jQuery('[name=currencies]').val());
  jQuery('.selected-currency').text(Currency.currentCurrency);
};

jQuery('.selected-currency').text(Currency.currentCurrency);

jQuery('.single-option-selector').change(function() {
    mlvedaload();
  });

$(document).ajaxComplete(function(event, xhr, settings) {
     if(settings.url.indexOf("cart.js")!=-1) {
       setTimeout(function() {
         mlvedaload();
       }, 1000);
     }
   });
}
</script>



  
<script type="text/javascript"> 
     var _learnq = _learnq || [];
     _learnq.push(['account', 'iBPeH3']); 
     (function () { 
     var b = document.createElement('script'); b.type = 'text/javascript'; b.async = true; 
     b.src = ('https:' == document.location.protocol ? 'https://' : 'http://') +     'a.klaviyo.com/media/js/analytics/analytics.js'; 
     var a = document.getElementsByTagName('script')[0]; a.parentNode.insertBefore(b, a);
     })(); 
</script>
  
  <!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 875366787;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="./acerca-de_files/conversion.js.descarga">
</script><iframe name="google_conversion_frame" title="Google conversion frame" width="300" height="13" src="./acerca-de_files/saved_resource.html" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no"></iframe>
<noscript>
&lt;div style="display:inline;"&gt;
&lt;img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/875366787/?value=0&amp;amp;guid=ON&amp;amp;script=0"/&gt;
&lt;/div&gt;
</noscript>

  <!-- google dynamic remarketing tag for theme.liquid -->
    
        <script type="text/javascript">
            var google_tag_params = {
            	ecomm_prodid: '',
            	ecomm_pagetype: 'other',
            	ecomm_totalvalue: 0
            };
        </script>
	

  
  
<iframe src="./acerca-de_files/dialog.html" scrolling="no" tabindex="-1" aria-hidden="true" style="position: fixed; top: 0px; left: 0px; z-index: 99999; height: 0px; width: 0px; border: 0px;"></iframe><div class="trustedsite-floating-element" style="z-index: 1000001; position: fixed; right: 0px; bottom: 0px; border-top-left-radius: 3px; margin: 0px !important; padding: 0px !important; border: 0px !important; max-width: none !important; max-height: none !important; width: 36px !important; height: 40px !important; background: rgb(255, 255, 255) !important; opacity: 0.7 !important;"></div><div class="trustedsite-floating-element" id="trustedsite-f3761753e4004963bc6c3f281dfb567b-image" style="position: fixed; width: 100px; overflow: hidden; bottom: 0px; right: -56px; margin: 0px !important; padding: 0px !important; border: 0px !important; background: none !important; max-width: none !important; max-height: none !important; height: 40px !important; z-index: 1000003 !important; cursor: pointer !important;"><img src="./acerca-de_files/float2-right.png" width="100" height="40" style="margin:0 !important;padding:0 !important;border:0 !important;background:none !important;max-width:none !important;max-height:none !important;width:100px !important;height:40px !important;" alt="McAfee SECURE"></div><div id="goog-gt-tt" class="skiptranslate" dir="ltr"><div style="padding: 8px;"><div><div class="logo"><img src="./acerca-de_files/translate_24dp.png" width="20" height="20" alt="Google Traductor de Google"></div></div></div><div class="top" style="padding: 8px; float: left; width: 100%;"><h1 class="title gray">Texto original</h1></div><div class="middle" style="padding: 8px;"><div class="original-text"></div></div><div class="bottom" style="padding: 8px;"><div class="activity-links"><span class="activity-link">Sugiere una traducción mejor</span><span class="activity-link"></span></div><div class="started-activity-container"><hr style="color: #CCC; background-color: #CCC; height: 1px; border: none;"><div class="activity-root"></div></div></div><div class="status-message" style="display: none;"></div></div><img id="trustedsite-f3761753e4004963bc6c3f281dfb567b-image-bg" src="./acerca-de_files/tm-float-bg-right-bottom.png" alt="McAfee SECURE" class="trustedsite-floating-element" style="width: 100px; right: -56px; margin: 0px !important; padding: 0px !important; border: 0px !important; background: none !important; max-width: none !important; max-height: none !important; position: fixed !important; height: 40px !important; z-index: 1000002 !important; bottom: 0px !important; display: none;"><div class="goog-te-spinner-pos"><div class="goog-te-spinner-animation"><svg xmlns="http://www.w3.org/2000/svg" class="goog-te-spinner" width="96px" height="96px" viewBox="0 0 66 66"><circle class="goog-te-spinner-path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg></div></div></body></html>