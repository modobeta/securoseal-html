<!DOCTYPE html>
<html class="js">
<head>
    <?php include('includes/meta.php'); ?>

    <title>
        Luggage Straps | Security Seals | Tamper Resistant - Securoseal
    </title>

    <?php include('includes/links.php'); ?>
</head>

<body id="tamper-evident-tape-tamper-evident-bag-security-seals-securoseal" class="template-page FullerSansDT-Regular">
    <header class="site-header header-bg" role="banner">
        <div class="mobile-header visible-xs visible-sm">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <button class="btn btn-link btn-mobile-nav toggle-mobile-nav"><i class="fa fa-bars"></i></button>
                    </div>

                    <div class="col-xs-6">
                        <div class="text-center">
                            <a class="mobile-logo" href="https://securoseal.com/">
                                <img src="dist/img/logo.png" alt="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-3"></div>
                </div>
            </div>
        </div>

        <div class="header-top hidden-xs FullerSansDT-Bold hidden-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-lg-2 text-center-sm">
                        <a id="logo" href="https://securoseal.com/">
                            <img src="dist/img/logo.png" alt="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
                        </a>
                    </div>

                    <div class="col-md-10 col-lg-10 text-center-sm">
                        <?php include('includes/menu.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <main class="site-main-content" role="main">
        <div class="breadcrumbs">
            <div class="container">
                <p>
                    <a href="https://securoseal.com/" class="homepage-link" title="Back to the frontpage">Home</a>
                    <span class="separator">›</span>
                    <span class="page-title">How It Works</span>
                </p>
            </div>
        </div>

        <div id="top" class="top-slider grey-bg how-hero">
            <div class="container">
                <div class="how-hero-text col-sm-5">
                    <h1 class="Grottel-Light orange-text top100">Luggage security<br> made SIMPLE.</h1>
                    <a href="https://securoseal.com/pages/how-securoseal-works-1" class="orange-text button-img FullerSansDT-Bold">
                        <div class="cf-play">
                            <img class="bottom" src="dist/img/play_over.png" alt="">
                            <img class="top" src="dist/img/play.png" alt="">
                        </div>
                        <span>HOW SECUROSEAL WORKS</span>
                    </a>
                </div>

                <img class="how-img col-sm-7" src="dist/img/how-it-works-hero.png">
            </div>
        </div>

        <div class="page-nav menu">
            <div class="container">
                <div class="col-md-3 hidden-sm hidden-xs">
                    <a href="https://securoseal.com/pages/how-it-works#logo">
                        <div class="cf-play sub-nav">
                            <img class="bottom" src="dist/img/orange-gear.png" alt="">
                            <img class="top" src="dist/img/grey-gear.png" alt="">
                            <span class="header-link">How It Works</span>
                        </div>
                    </a>
                </div>

                <div class="col-sm-12 col-md-9">
                    <div class="sub-page-nav">
                        <a href="#protection">Protection</a>
                        <a href="#tamper-indicators">Tamper Indicators</a>
                        <a href="#reliability">Reliability</a>
                        <a href="#superior">Superior Protection</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="protection" class="headlines dark-bg thin">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 hidden-xs">
                    <img src="dist/img/page2_screen2.png" alt="">
                    </div>
                    <div class="col-sm-7 bottom50">
                        <h1 class="Grottel-Light white-text">How Securoseal Protects.</h1>

                        <ul>
                            <li class="check-in"> <img src="dist/img/case.png"><p>Strap your suitcase closed, seal your zips and keep a numbered receipt.</p></li>
                            <li class="check-in"> <img src="dist/img/zipper.png"><p>Moving zips can be opened and resealed in seconds. Securoseal isolates zips.</p></li>
                            <li class="check-in"> <img src="dist/img/lock.png"><p>Stronger than 2x your maximum check in weight. Securoseal stays sealed.</p></li>
                            <li class="check-in"> <img src="dist/img/bang.png"><p>You can write on each Securoseal to assist you in reclaiming your luggage.</p></li>
                            <li class="check-in"> <img src="dist/img/open_lock.png"><p>Tamper evident technology helps you detect a tampering event.</p></li>
                            <li class="check-in"> <img src="dist/img/scizzors.png"><p>Travel friendly - Securoseal can be released without scissors upon arrival.</p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="tamper-indicators" class="tampering orange-bg">
            <div id="slick-container-tamp" class="container">
        <div class="tamper-slick-wrap">
            <div id="home-slide-1" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light white-text top50">Tamper Indicators</h1>

                    <p class="text-white">Each Securoseal device is engineered to be tough enough to survive transportation and sensitive enough to detect tampering. Every device features a unique ‘5 phase’ tamper detection system to protect you.<br><br>
                        See how it works:</p>
                </div>

                <div class="col-md-6"></div>

                <img src=" dist/img/page2_screen3_panel1.png" alt="">
            </div>

            <div id="home-slide-2" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light white-text top50">Tamper evident <br>surface seal.</h1>
                    <img class="instruction-img" src="dist/img/number-1.png">
                    <p>Highly sensitive tamper indication with a unique identity number. Tampering in any direction will create a void tamper pattern.</p>
                </div>
                <div  class="col-md-6"></div>

                <img src=" dist/img/page2_screen3_panel2.png" alt="">
            </div>

            <div id="home-slide-3" class="slide-wrap ">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light white-text top50">Tamper evident <br>load bearing seal.</h1>
                    <img class="instruction-img" src="dist/img/number-2.png">
                    <p>Strong enough to hold double the maximum checked weight limit for luggage. Once sealed, tampering in any direction will cause the surface to fragment.</p>
                </div>

                <div  class="col-md-6"></div>

                <img src=" dist/img/page2_screen3_panel3.png" alt="">
            </div>

            <div id="home-slide-4" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light white-text top50">Tamper evident <br>zip seal.</h1>
                    <img class="instruction-img" src="dist/img/number-3.png">
                    <p>Isolate zips with a single use cable tie. Includes an internal ‘one way’ metal sealing device.
                        Numbered &amp; bar coded to match the unique identity number of your seal. Once sealed, attempted removal will leave tamper evidence on the cable tie.</p>
                </div>
                <div  class="col-md-6"></div>

                <img src=" dist/img/page2_screen3_panel4.png" alt="">
            </div>

            <div id="home-slide-5" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light white-text top50">Identification &amp; <br>receipt system.</h1>
                    <img class="instruction-img" src="dist/img/number-4.png">
                    <p>Each seal is marked with a unique identity number and includes a tamper evident receipt
                        that is adhesive. Keep the receipt with you to verify the identity number of your seal.</p>
                </div>
                <div  class="col-md-6"></div>

                <img src=" dist/img/page2_screen3_panel5.png" alt="">
            </div>

            <div id="home-slide-6" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light white-text top50">Single use <br>buckle release.</h1>
                    <img class="instruction-img" src="dist/img/number-5.png">
                    <p>At your destination, your seal can be released without cutting tools. Use of this function will leave tamper evidence on the buckle.</p>
                </div>

                <div  class="col-md-6"></div>

                <img src=" dist/img/page2_screen3_panel6.png" alt="">
            </div>
        </div>
    </div>
        </div>
  
        <div id="reliability" class="problem">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="dist/img/page2_screen4.png" alt="">
                    </div>

                    <div class="col-sm-6">
                        <h1 class="Grottel-Light white-text top40">Proven Reliability.</h1>
                        <p class="font-pal reliability-text"><span class="white-text">Airport tested, used by thousands daily.</span><br> Securoseal is a proven product. Airport authorities have successfully tested it in the USA, Australia, China and Colombia. Our product made <a href="https://securoseal.myshopify.com/blogs/news/south-africa-securoseal-product-foils-baggage-thieves">headlines</a> when Airports Company South Africa chose Securoseal to safeguard baggage during FIFA World Cup 2010. In <a href="https://www.send2press.com/newswire/New-Tamper-Evident-Baggage-Security-Initiative-Offered-at-El-Dorado-Airport-Bogota-Colombia_2014-04-0414-004.shtml" target="_blank">2014</a> Securoseal became the sole luggage security product offered to millions of departing passengers in Bogota Colombia’s new international airport. Every day, we ship products purchased online worldwide. Join the growing number of passengers that choose Securoseal. </p>
                    </div>
                </div>
            </div>
        </div>

        <div id="superior" class="tampering grey-bg">
            <div id="slick-container" class="container">
                <div class="slick-wrap">
                    <div id="home-slide-1" class="slide-wrap">
                        <div class="slide-text1 col-md-6">
                            <h1 class="Grottel-Light orange-text top50">Gold Standard:<br> Here's why.</h1>
                            <p><span class="grey-text">Others might claim that title. We earned it.</span>
                                <br>Before you purchase any tamper evident product, make an informed decision and ask these questions:</p>
                        </div>

                        <div class="col-md-6"></div>

                        <img src=" dist/img/page2_screen5_panel1.png" alt="">
                    </div>

                    <div id="home-slide-2" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light orange-text top50">Is it strong enough for your luggage?</h1>

                    <p>
                        Securoseal’s band is strong enough to carry more than two suitcases, each loaded with maximum check in weight. It is the strongest tamper evident product in the field.</p>
                </div>

                <div class="col-md-6"></div>

                <img src="dist/img/page2_screen5_panel2.png" alt="">
            </div>

                    <div id="home-slide-3" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light orange-text top50">Can it survive baggage handling?</h1>

                    <p>The same sealed bag was circulated in Sydney Airport’s baggage handling system continuously for 2 weeks. They gave up before our seal did.</p>
                </div>

                <div  class="col-md-6"></div>

                <img src="dist/img/page2_screen5_panel3.png" alt="">
            </div>

                    <div id="home-slide-4" class="slide-wrap">
                        <div class="slide-text1 col-md-6">
                            <h1 class="Grottel-Light orange-text top50">Is it proven reliable in the field?</h1>

                            <p>Securoseal’s reliability is proven. Our product has been years in the field, used to seal hundreds of thousands of bags and relied upon by airport authorities, airlines and passengers alike.</p>
                        </div>

                        <div  class="col-md-6"></div>

                        <img src="dist/img/page2_screen5_panel4.png" alt="">
                    </div>

                    <div id="home-slide-5" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light orange-text top50">Is the product quality consistent?</h1>
                    <p>Securoseal’s quality is guaranteed. No shortcuts here. We use premium materials by DuPont and 3M in our own dedicated manufacturing facility.</p>
                </div>

                <div  class="col-md-6"></div>

                <img src="dist/img/page2_screen5_panel5.png" alt="">
            </div>

                    <div id="home-slide-6" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light orange-text top50">Does the product have a track record?</h1>

                    <p>Our product has been used by authorities to disrupt criminal activity in airports. The successful results made headlines. <span class="grey-text">Enough said.</span></p>

                    <a href="https://securoseal.myshopify.com/blogs/news/south-africa-securoseal-product-foils-baggage-thieves">
                        <div class="cf-play sub-nav">
                            <img class="bottom" src="dist/img/button_read_over.png" alt="">
                            <img class="top" src="dist/img/button_read.png" alt="">
                            <span class="header-link how">READ THE ARTICLE</span>
                        </div>
                    </a>
                </div>

                <div  class="col-md-6"></div>

                <img src="dist/img/page2_screen5_panel6.png" alt="">
            </div>

                    <div id="home-slide-7" class="slide-wrap">
                <div class="slide-text1 col-md-6">
                    <h1 class="Grottel-Light orange-text top50">Don’t settle for less.</h1>

                    <p>To safeguard your belongings and protect your legal rights, you deserve the best. Choose Securoseal.</p>
                </div>

                <div class="col-md-6"></div>

                <img src="dist/img/page2_screen5_panel7.png" alt="">
            </div>
                </div>
            </div>
        </div>

        <div class="chance">
	        <div class="container">
		        <div class="row">
                    <div class="col-sm-6">
                        <h1 class="Grottel-Light top100">Don't chance it.<br>Get protection.</h1>

                        <p class="font-pal top20">Buy the Securoseal tamper evident luggage seal today.</p>

                        <a href="https://securoseal.com/collections/all" class="orange-text orange-hollow top10">BUY NOW</a>
                    </div>

                    <div id="chance-right" class="col-sm-6">
                        <img id="secure-prod" class="bottom50 hidden-sm" src="dist/img/secure-prod.png" alt="">

                        <div class="desc-img bottom50">
                            <a href="https://securoseal.myshopify.com/">
                                <p class="chance-text FullerSansDT-Bold">WHY SECUROSEAL</p>

                                <div class="cf">
                                    <img class="botom" src="dist/img/btn_nav_whysecuroseal_over.png" alt="">
                                    <img class="top" src="dist/img/btn_nav_whysecuroseal.png" alt="">
                                </div>
                            </a>

                            <a href="https://securoseal.myshopify.com/pages/how-to-use">
                                <p class="chance-text FullerSansDT-Bold">HOW TO USE</p>

                                <div class="cf">
                                    <img class="bottom" src="dist/img/use-clear.png" alt="">
                                    <img class="top" src="dist/img/use.png" alt="">
                                </div>
                            </a>

                            <a href="https://securoseal.myshopify.com/pages/about-us">
                                <p class="chance-text FullerSansDT-Bold">ABOUT</p>

                                <div class="cf">
                                    <img class="botom" src="dist/img/about-clear.png" alt="">
                                    <img class="top" src="dist/img/about.png" alt="">
                                </div>
                            </a>
                        </div>
                    </div>
  		        </div>
	        </div>
        </div>
    </main>

    <footer class="site-footer dark-bg" role="contentinfo">
        <?php include('includes/footer.php'); ?>
    </footer>

    <div class="mobile-nav-wrap hidden-md hidden-lg">
        <a class="mobile-nav-close toggle-mobile-nav" href="#"><i class="fa fa-times"></i></a>

        <?php include('includes/menu-movil.php'); ?>
    </div>

    <?php include('includes/javascript.php'); ?>
</body>
</html>