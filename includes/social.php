<ul class="social-m">
    <li>
        <a href="https://www.facebook.com/Securoseal-1545230415730067/">
            <div class="cf-play">
                <img class="bottom" src="dist/img/social_facebook_over.png" alt="">
                <img class="top" src="dist/img/social_facebook.png" alt="">
            </div>
        </a>
    </li>

    <li>
        <a href="https://plus.google.com/116459602198520644377/posts">
            <div class="cf-play">
                <img class="bottom" src="dist/img/social_googleplus_over.png" alt="">
                <img class="top" src="dist/img/social_googleplus.png" alt="">
            </div>
        </a>
    </li>

    <li>
        <a href="https://twitter.com/securoseal">
            <div class="cf-play">
                <img class="bottom" src="dist/img/social_twitter_over.png" alt="">
                <img class="top" src="dist/img/social_twitter.png" alt="">
            </div>
        </a>
    </li>

    <li>
        <a href="https://www.youtube.com/user/securoseal">
            <div class="cf-play">
                <img class="bottom" src="dist/img/social_youtube_over.png" alt="">
                <img class="top" src="dist/img/social_youtube.png" alt="">
            </div>
        </a>
    </li>
</ul>