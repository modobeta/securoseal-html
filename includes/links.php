<link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
<link href="dist/css/fuentes.css" rel="stylesheet" type="text/css">
<link href="dist/css/style.scss.css" rel="stylesheet" type="text/css" media="all">
<link href="dist/css/custom.scss.css" rel="stylesheet" type="text/css" media="all">
<link href="dist/css/slick.css" rel="stylesheet" type="text/css" media="all">
<link href="dist/css/slick-theme.css" rel="stylesheet" type="text/css" media="all">
<link href="dist/css/social-icons.css" rel="stylesheet" type="text/css" media="all">
<link href="dist/css/font-awesome.css" rel="stylesheet" type="text/css" media="all">
<link href="dist/css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">
<link href="dist/css/mlveda-currencies-switcher-format.css" rel="stylesheet" type="text/css" media="all">
<link href="dist/css/MyFontsWebfontsKit.css" rel="stylesheet" type="text/css" media="all">

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
<![endif]-->