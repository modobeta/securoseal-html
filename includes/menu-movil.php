<nav role="navigation">
    <h3 class="mobile-nav-heading">Pages</h3>

    <ul class="mobile-nav">
        <li class="active first">
            <a href="index.php">Why Securoseal?</a>
        </li>

        <li>
            <a href="como-funciona.php">How It Works</a>
        </li>

        <li class="">
            <a href="como-usarlo.php">How To Use</a>
        </li>

        <li class="">
            <a href="acerca-de.php">About Us</a>
        </li>
    </ul>
</nav>