<div class="container">
    <div class="row footer-row1">
        <div class="col-sm-6">
            <a href="#logo">
                <div class="cf-play">
                    <img class="bottom" src="dist/img/large-orange-gear.png" alt="">
                    <img class="top" src="dist/img/large-white-gear.png" alt="">
                </div>
            </a>

            <p class="white-text">SECUROSEAL - TAMPER EVIDENT LUGGAGE SEAL</p>
        </div>

        <div class="col-sm-6 text-right">
            <?php include('includes/social.php'); ?>
        </div>
    </div>

    <div class="row footer-row2">
        <div class="col-sm-6">
            <small>Copyright © 2009-2016 Securoseal. All Rights Reserved.</small>
        </div>

        <div class="col-sm-6 text-right hidden">
            <ul class="footer-links">
                <li><a href="https://securoseal.com/pages/privacy-policy">Privacy</a></li>

                <li><a href="https://securoseal.com/pages/terms-of-use">Terms of Use</a></li>
            </ul>
        </div>
    </div>
</div>