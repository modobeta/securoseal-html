<!-- JavaScript -->
<script src="dist/js/jquery.min.js" type="text/javascript"></script>
<script>jQuery('html').removeClass('no-js').addClass('js');</script>
<script src="dist/js/jquery.fancybox.js" type="text/javascript"></script>
<script src="dist/js/jquery.swipebox.min.js" type="text/javascript"></script>
<script src="dist/js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="dist/js/shop.js" type="text/javascript"></script>
<script src="dist/js/slick.min.js" type="text/javascript"></script>
<script type="text/javascript" src="dist/js/host-loader.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
