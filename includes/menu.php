<nav role="navigation">
    <ul class="main-nav topper clearfix">
        <li class="active first">
            <a href="#logo">Why Securoseal?</a>
        </li>

        <li>
            <a href="como-funciona.php">How It Works</a>
        </li>

        <li>
            <a href="como-usarlo.php">How To Use</a>
        </li>

        <li>
            <a href="acerca-de.php">About Us</a>
        </li>
    </ul>
</nav>