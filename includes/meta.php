<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:image" content="dist/img/logo.png">
<meta property="og:image:secure_url" content="dist/img/logo.png">
<meta property="og:title" content="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
<meta property="og:type" content="website">
<meta property="og:url" content="https://securoseal.com/">
<meta property="og:site_name" content="Luggage Straps | Security Seals | Tamper Resistant - Securoseal">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' /><![endif]-->